﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

using Confluence;
using log4net;
using Page = System.Windows.Controls.Page;

namespace GreenshotConfluencePlugin {
	/// <summary>
	/// Interaction logic for ConfluenceTreePicker.xaml
	/// </summary>
	public partial class ConfluenceTreePicker : Page {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ConfluenceTreePicker));
		private readonly ConfluenceConnector _confluenceConnector;
		private readonly ConfluenceUpload _confluenceUpload;
		private bool _isInitDone;
		
		public ConfluenceTreePicker(ConfluenceUpload confluenceUpload) {
			_confluenceConnector = ConfluencePlugin.ConfluenceConnector;
			_confluenceUpload = confluenceUpload;
			InitializeComponent();
		}

		void pageTreeViewItem_DoubleClick(object sender, MouseButtonEventArgs eventArgs) {
			LOG.Debug("spaceTreeViewItem_MouseLeftButtonDown is called!");
			TreeViewItem clickedItem = eventArgs.Source as TreeViewItem;
			if (clickedItem ==null) {
				return;
			}
			Confluence.Page page = clickedItem.Tag as Confluence.Page;
			if (page == null) {
				return;
			}
			if (!clickedItem.HasItems) {
				LOG.Debug("Loading pages for page: " + page.Title);
				(new Thread(() => {
					Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)(() => {ShowBusy.Visibility = Visibility.Visible;}));
					List<Confluence.Page> pages = _confluenceConnector.GetPageChildren(page);
					Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)(() => {
						foreach(Confluence.Page childPage in pages) {
							LOG.Debug("Adding page: " + childPage.Title);
							TreeViewItem pageTreeViewItem = new TreeViewItem();
							pageTreeViewItem.Header = childPage.Title;
							pageTreeViewItem.Tag = childPage;
							clickedItem.Items.Add(pageTreeViewItem);
							pageTreeViewItem.PreviewMouseDoubleClick += pageTreeViewItem_DoubleClick;
							pageTreeViewItem.PreviewMouseLeftButtonDown += pageTreeViewItem_Click;
						}
						ShowBusy.Visibility = Visibility.Collapsed;
					}));
				 }
				) { Name = "Loading childpages for confluence page " + page.Title }).Start();
			}
		}
		
		void pageTreeViewItem_Click(object sender, MouseButtonEventArgs eventArgs) {
			LOG.Debug("pageTreeViewItem_PreviewMouseDoubleClick is called!");
			TreeViewItem clickedItem = eventArgs.Source as TreeViewItem;
			if (clickedItem ==null) {
				return;
			}
			Confluence.Page page = clickedItem.Tag as Confluence.Page;
			_confluenceUpload.SelectedPage = page;
			if (page != null) {
				LOG.Debug("Page selected: " + page.Title);
			}
		}

		void Page_Loaded(object sender, RoutedEventArgs e) {
			_confluenceUpload.SelectedPage = null;
			if (_isInitDone) {
				return;
			}
			ShowBusy.Visibility = Visibility.Visible;
			(new Thread(() => {
				Dispatcher.BeginInvoke(DispatcherPriority.Normal, (ThreadStart)(() => {
					List<Space> spaces = _confluenceUpload.Spaces;
					foreach (Space space in spaces) {
						TreeViewItem spaceTreeViewItem = new TreeViewItem();
						spaceTreeViewItem.Header = space.Name;
						spaceTreeViewItem.Tag = space;
		
						// Get homepage
						try {
							Confluence.Page page = _confluenceConnector.GetSpaceHomepage(space);
							TreeViewItem pageTreeViewItem = new TreeViewItem();
							pageTreeViewItem.Header = page.Title;
							pageTreeViewItem.Tag = page;
							pageTreeViewItem.PreviewMouseDoubleClick += pageTreeViewItem_DoubleClick;
							pageTreeViewItem.PreviewMouseLeftButtonDown += pageTreeViewItem_Click;
							spaceTreeViewItem.Items.Add(pageTreeViewItem);
							ConfluenceTreeView.Items.Add(spaceTreeViewItem);
						} catch (Exception ex) {
							LOG.Error("Can't get homepage for space : " + space.Name + " (" + ex.Message + ")");
						}
					}
					ShowBusy.Visibility = Visibility.Collapsed;
					_isInitDone = true;
				}));
			 }
			) { Name = "Loading spaces for confluence"}).Start();
		}
	}
}