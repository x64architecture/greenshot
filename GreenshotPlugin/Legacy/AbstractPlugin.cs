﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.Collections.Generic;

namespace GreenshotPlugin.Core {
    public abstract class AbstractPlugin : IGreenshotPlugin, IDisposable {
        public virtual void Dispose() {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        ///     Implementation of the IGreenshotPlugin.Initialize
        /// </summary>
		/// <param name="pluginHost">Use the IGreenshotPluginHost interface to register events</param>
        /// <param name="metadata">IDictionary string, string</param>
        public abstract bool Initialize(IGreenshotHost pluginHost, IDictionary<string, object> metadata);

        /// <summary>
        ///     Implementation of the IPlugin.Shutdown
        /// </summary>
        public virtual void Shutdown() {
            // Do nothing
        }

        /// <summary>
        ///     Implementation of the IPlugin.Configure
        /// </summary>
        public virtual void Configure() {
        }

        /// <summary>
        ///     Implementation of the IPlugin.Configurable
        /// </summary>
        public virtual bool Configurable {
            get { return true; }
        }

        /// <summary>
        ///     Implementation of the IPlugin.Destinations
        /// </summary>
        public virtual IEnumerable<ILegacyDestination> Destinations() {
            yield break;
        }

        /// <summary>
        ///     Implementation of the IPlugin.Processors
        /// </summary>
        public virtual IEnumerable<ILegacyProcessor> Processors() {
            yield break;
        }

        protected virtual void Dispose(bool disposing) {
        }
    }
}