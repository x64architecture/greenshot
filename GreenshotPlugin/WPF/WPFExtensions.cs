﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace GreenshotPlugin.WPF {
	public static class WPFExtensions {
		public static System.Drawing.Rectangle ToRectangle(this Rect rect) {
			return new System.Drawing.Rectangle((int)rect.Left, (int)rect.Top, (int)rect.Width, (int)rect.Height);
		}
		public static Rect ToRect(this System.Drawing.Rectangle rectangle) {
			return new Rect(rectangle.Left, rectangle.Top, rectangle.Width, rectangle.Height);
		}

		/// <summary>
		/// Copy the rect from source to target 
		/// </summary>
		/// <param name="target"></param>
		/// <param name="source"></param>
		/// <param name="rect"></param>
		public static void CopyPixels(this WriteableBitmap target, BitmapSource source, Rect rect) {
			// Calculate stride of source
			int stride = source.PixelWidth * (source.Format.BitsPerPixel / 8);

			// Create data array to hold source pixel data
			byte[] data = new byte[stride * source.PixelHeight];

			// Copy source image pixels to the data array
			source.CopyPixels(data, stride, 0);

			// Write the pixel data to the WriteableBitmap.
			target.WritePixels(new Int32Rect(0, 0, source.PixelWidth, source.PixelHeight), data, stride, 0);
		}

		/// <summary>
		/// Make a BitmapSource (actually a RenderTargetBitmap) from a DrawingImage
		/// </summary>
		/// <param name="drawingImage"></param>
		/// <returns>BitmapSource</returns>
		public static BitmapSource ToBitmapSource(this DrawingImage drawingImage) {
			return drawingImage.ToBitmapSource((int)(drawingImage.Width + 0.5), (int)(drawingImage.Height + 0.5));
		}

		/// <summary>
		/// Make a BitmapSource (actually a RenderTargetBitmap) from a DrawingImage
		/// </summary>
		/// <param name="drawingImage"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns>BitmapSource</returns>
		public static BitmapSource ToBitmapSource(this DrawingImage drawingImage, int width, int height) {
			var renderTargetBitmap = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);
			var image = new Image();
			image.Source = drawingImage;
			image.Arrange(new Rect(0, 0, width, height));
			image.UpdateLayout();
			renderTargetBitmap.Render(image);
			return renderTargetBitmap;
		}

		/// <summary>
		/// Convert an ImageSource to a bitmap, also works for DrawingImage (like the one for SVG)
		/// </summary>
		/// <param name="imageSource">ImageSource</param>
		/// <returns>Bitmap</returns>
		public static System.Drawing.Bitmap ToBitmap(this ImageSource imageSource) {
			return imageSource.ToBitmap((int)(imageSource.Width + 0.5), (int)(imageSource.Height + 0.5));
		}

		/// <summary>
		/// Convert a BitmapSource to a System.Drawing.Icon
		/// </summary>
		/// <param name="imageSource">BitmapSource</param>
		/// <returns>Icon</returns>
		public static System.Drawing.Icon ToIcon(this BitmapSource bitmapSource) {
			var stream = new MemoryStream();
			IconBitmapEncoder encoder = new IconBitmapEncoder();
			encoder.Frames.Add(BitmapFrame.Create(bitmapSource));
			encoder.Save(stream);
			return new System.Drawing.Icon(stream);
		}

		/// <summary>
		/// Convert an ImageSource to a bitmap, also works for DrawingImage (like the one for SVG)
		/// </summary>
		/// <param name="imageSource">ImageSource</param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns>Bitmap</returns>
		public static System.Drawing.Bitmap ToBitmap(this ImageSource imageSource, int width, int height) {
			if (imageSource == null) {
				return null;
			}

			var image = new Image();
			image.Source = imageSource;
			image.Arrange(new Rect(0, 0, width, height));
			image.UpdateLayout();
			return image.ToBitmap(width, height);
		}

		/// <summary>
		/// Render a FrameworkElement to a bitmap, using it's current size
		/// </summary>
		/// <param name="frameworkElement"></param>
		/// <returns>System.Drawing.Bitmap</returns>
		public static System.Drawing.Bitmap ToBitmap(this FrameworkElement frameworkElement) {
			return frameworkElement.ToBitmap((int)(frameworkElement.Width + 0.5), (int)(frameworkElement.Height + 0.5));
		}

		/// <summary>
		/// Render a FrameworkElement to a bitmap, with the supplied width & height
		/// </summary>
		/// <param name="frameworkElement"></param>
		/// <param name="width"></param>
		/// <param name="height"></param>
		/// <returns>System.Drawing.Bitmap</returns>
		public static System.Drawing.Bitmap ToBitmap(this FrameworkElement frameworkElement, int width, int height) {
			var renderTargetBitmap = new RenderTargetBitmap(width, height, 96, 96, PixelFormats.Pbgra32);
			renderTargetBitmap.Render(ModifyToDrawingVisual(frameworkElement, width, height));

			// Use PNG to we maintain transparency!
			var encoder = new PngBitmapEncoder();
			encoder.Frames.Add(BitmapFrame.Create(renderTargetBitmap));

			var stream = new MemoryStream();
			encoder.Save(stream);

			return new System.Drawing.Bitmap(stream);
		}

		/// <summary>
		/// Helper method for rendering a FrameworkElement
		/// Modify Position aranges and measures the FrameworkElement so it can be drawn.
		/// </summary>
		/// <param name="fe"></param>
		private static void ModifyPosition(FrameworkElement fe) {
			/// get the size of the visual with margin
			Size fs = new Size(fe.ActualWidth + fe.Margin.Left + fe.Margin.Right, fe.ActualHeight + fe.Margin.Top + fe.Margin.Bottom);

			/// measure the visual with new size
			fe.Measure(fs);

			/// arrange the visual to align parent with (0,0)
			fe.Arrange(new Rect(-fe.Margin.Left, -fe.Margin.Top, fs.Width, fs.Height));
		}

		/// <summary>
		/// "Reverts" the ModifyPosition call
		/// </summary>
		/// <param name="fe"></param>
		private static void ModifyPositionBack(FrameworkElement fe) {
			/// remeasure a size smaller than need, wpf will
			/// rearrange it to the original position
			fe.Measure(new Size());
		}

		private static DrawingVisual ModifyToDrawingVisual(Visual visual, int width, int height) {
			/// new a drawing visual and get its context
			DrawingVisual dv = new DrawingVisual();
			DrawingContext dc = dv.RenderOpen();

			/// generate a visual brush by input, and paint
			VisualBrush vb = new VisualBrush(visual);
			dc.DrawRectangle(vb, null, new Rect(new Point(), new Size(width, height)));
			dc.Close();

			return dv;
		}
	}
}
