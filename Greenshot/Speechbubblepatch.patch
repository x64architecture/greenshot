Index: Greenshot/Drawing/DrawableContainer.cs
===================================================================
--- Greenshot/Drawing/DrawableContainer.cs	(revision 2434)
+++ Greenshot/Drawing/DrawableContainer.cs	(working copy)
@@ -72,6 +72,8 @@
 		}
 		[NonSerialized]
 		protected Gripper[] grippers;
+		protected Gripper targetGripper;
+
 		private bool layoutSuspended = false;
 		
 		[NonSerialized]
@@ -287,6 +289,24 @@
 				parent.Controls.AddRange(grippers); // otherwise we'll attach them in switchParent
 			}
 		}
+
+		/// <summary>
+		/// Initialize a target gripper
+		/// </summary>
+		protected void InitTargetGripper(Color gripperColor, Point location) {
+			targetGripper = new Gripper();
+			targetGripper.Cursor = Cursors.SizeAll;
+			targetGripper.BackColor = gripperColor;
+			targetGripper.MouseDown += new MouseEventHandler(gripperMouseDown);
+			targetGripper.MouseUp += new MouseEventHandler(gripperMouseUp);
+			targetGripper.MouseMove += new MouseEventHandler(gripperMouseMove);
+			targetGripper.Visible = false;
+			targetGripper.Parent = parent;
+			targetGripper.Location = location;
+			if (parent != null) {
+				parent.Controls.Add(targetGripper); // otherwise we'll attach them in switchParent
+			}
+		}
 		
 		public void SuspendLayout() {
 			layoutSuspended = true;
@@ -347,22 +367,45 @@
 		private void gripperMouseDown(object sender, MouseEventArgs e) {
 			mx = e.X;
 			my = e.Y;
-			Status = EditStatus.RESIZING;
-			boundsBeforeResize = new Rectangle(left, top, width, height);
-			boundsAfterResize = new RectangleF(boundsBeforeResize.Left, boundsBeforeResize.Top, boundsBeforeResize.Width, boundsBeforeResize.Height);
+			Gripper originatingGripper = (Gripper)sender;
+			if (originatingGripper != targetGripper) {
+				Status = EditStatus.RESIZING;
+				boundsBeforeResize = new Rectangle(left, top, width, height);
+				boundsAfterResize = new RectangleF(boundsBeforeResize.Left, boundsBeforeResize.Top, boundsBeforeResize.Width, boundsBeforeResize.Height);
+			} else {
+				Status = EditStatus.MOVING;
+			}
 			isMadeUndoable = false;
 		}
 
 		private void gripperMouseUp(object sender, MouseEventArgs e) {
+			Gripper originatingGripper = (Gripper)sender;
+			if (originatingGripper != targetGripper) {
+				boundsBeforeResize = Rectangle.Empty;
+				boundsAfterResize = RectangleF.Empty;
+				isMadeUndoable = false;
+			}
 			Status = EditStatus.IDLE;
-			boundsBeforeResize = Rectangle.Empty;
-			boundsAfterResize = RectangleF.Empty;
-			isMadeUndoable = false;
 			Invalidate();
 		}
-		
+
+		/// <summary>
+		/// Should be overridden to handle gripper moves on the "TargetGripper"
+		/// </summary>
+		/// <param name="absX"></param>
+		/// <param name="absY"></param>
+		protected virtual void TargetGripperMove(int newX, int newY) {
+			targetGripper.Left = newX;
+			targetGripper.Top = newY;
+		}
+
 		private void gripperMouseMove(object sender, MouseEventArgs e) {
-			if(Status.Equals(EditStatus.RESIZING)) {
+			Gripper originatingGripper = (Gripper)sender;
+			int absX = originatingGripper.Left + e.X;
+			int absY = originatingGripper.Top + e.Y;
+			if (originatingGripper == targetGripper && Status.Equals(EditStatus.MOVING)) {
+				TargetGripperMove(absX, absY);
+			} else if (Status.Equals(EditStatus.RESIZING)) {
 				// check if we already made this undoable
 				if (!isMadeUndoable) {
 					// don't allow another undo until we are finished with this move
@@ -373,10 +416,6 @@
 				
 				Invalidate();
 				SuspendLayout();
-				
-				Gripper gr = (Gripper)sender;
-				int absX = gr.Left + e.X;
-				int absY = gr.Top + e.Y;
 
 				// reset "workbench" rectangle to current bounds
 				boundsAfterResize.X = boundsBeforeResize.X;
@@ -385,11 +424,11 @@
 				boundsAfterResize.Height = boundsBeforeResize.Height;
 
 				// calculate scaled rectangle
-				ScaleHelper.Scale(ref boundsAfterResize, gr.Position, new PointF(absX, absY), ScaleHelper.GetScaleOptions());
+				ScaleHelper.Scale(ref boundsAfterResize, originatingGripper.Position, new PointF(absX, absY), ScaleHelper.GetScaleOptions());
 
 				// apply scaled bounds to this DrawableContainer
 				ApplyBounds(boundsAfterResize);
-	            
+
 				ResumeLayout();
 				Invalidate();
 			}
@@ -456,9 +495,19 @@
 		
 		public virtual void ShowGrippers() {
 			for (int i=0; i<grippers.Length; i++) {
-				if(grippers[i].Enabled) grippers[i].Show();
-				else grippers[i].Hide();
+				if (grippers[i].Enabled) {
+					grippers[i].Show();
+				} else {
+					grippers[i].Hide();
+				}
 			}
+			if (targetGripper != null) {
+				if (targetGripper.Enabled) {
+					targetGripper.Show();
+				} else {
+					targetGripper.Hide();
+				}
+			}
 			this.ResumeLayout();
 		}
 		
@@ -467,6 +516,9 @@
 			for (int i=0; i<grippers.Length; i++) {
 				grippers[i].Hide();
 			}
+			if (targetGripper != null) {
+				targetGripper.Hide();
+			}
 		}
 		
 		public void ResizeTo(int width, int height, int anchorPosition) {
Index: Greenshot/Drawing/SpeechbubbleContainer.cs
===================================================================
--- Greenshot/Drawing/SpeechbubbleContainer.cs	(revision 0)
+++ Greenshot/Drawing/SpeechbubbleContainer.cs	(revision 0)
@@ -0,0 +1,188 @@
+/*
+ * Greenshot - a free and open source screenshot tool
+ * Copyright (C) 2007-2012  Thomas Braun, Jens Klingen, Robin Krom
+ * 
+ * For more information see: http://getgreenshot.org/
+ * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
+ * 
+ * This program is free software: you can redistribute it and/or modify
+ * it under the terms of the GNU General Public License as published by
+ * the Free Software Foundation, either version 1 of the License, or
+ * (at your option) any later version.
+ * 
+ * This program is distributed in the hope that it will be useful,
+ * but WITHOUT ANY WARRANTY; without even the implied warranty of
+ * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ * GNU General Public License for more details.
+ * 
+ * You should have received a copy of the GNU General Public License
+ * along with this program.  If not, see <http://www.gnu.org/licenses/>.
+ */
+using System;
+using System.Drawing;
+using System.Drawing.Drawing2D;
+
+using Greenshot.Drawing.Fields;
+using Greenshot.Helpers;
+using Greenshot.Plugin.Drawing;
+
+namespace Greenshot.Drawing {
+	/// <summary>
+	/// Description of SpeechbubbleContainer.
+	/// </summary>
+	[Serializable()] 
+	public class SpeechbubbleContainer : TextContainer {
+		private static readonly log4net.ILog LOG = log4net.LogManager.GetLogger(typeof(SpeechbubbleContainer));
+		public SpeechbubbleContainer(Surface parent) : base(parent) {
+			AddField(GetType(), FieldType.LINE_THICKNESS, 2);
+			AddField(GetType(), FieldType.LINE_COLOR, Color.Red);
+			AddField(GetType(), FieldType.FILL_COLOR, Color.Transparent);
+			AddField(GetType(), FieldType.SHADOW, true);
+		}
+
+		protected override void TargetGripperMove(int absX, int absY) {
+			base.TargetGripperMove(absX, absY);
+			Invalidate();
+		}
+		/// <summary>
+		/// Called from Surface (the parent) when the drawing begins (mouse-down)
+		/// </summary>
+		/// <returns>true if the surface doesn't need to handle the event</returns>
+		public override bool HandleMouseDown(int mouseX, int mouseY) {
+			if (targetGripper == null) {
+				InitTargetGripper(Color.Green, new Point(mouseX, mouseY));
+			}
+			return base.HandleMouseDown(mouseX, mouseY);
+		}
+
+		public override Rectangle DrawingBounds {
+			get {
+				return new Rectangle(0,0,parent.Width,parent.Height);
+			}
+		}
+		public override void Draw(Graphics graphics, RenderMode renderMode) {
+			if (targetGripper == null) {
+				return;
+			}
+			graphics.SmoothingMode = SmoothingMode.HighQuality;
+			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
+			graphics.CompositingQuality = CompositingQuality.HighQuality;
+			graphics.PixelOffsetMode = PixelOffsetMode.None;
+			
+			int lineThickness = GetFieldValueAsInt(FieldType.LINE_THICKNESS);
+			Color lineColor = GetFieldValueAsColor(FieldType.LINE_COLOR);
+			Color fillColor = GetFieldValueAsColor(FieldType.FILL_COLOR);
+			bool shadow = GetFieldValueAsBool(FieldType.SHADOW);
+			bool lineVisible = (lineThickness > 0 && Colors.IsVisible(lineColor));
+			Rectangle rect = GuiRectangle.GetGuiRectangle(this.Left, this.Top, this.Width, this.Height);
+
+			int tailAngle = 90 + (int)GeometryHelper.Angle2D(Left + (Width / 2), Top + (Height / 2), targetGripper.Left, targetGripper.Top);
+			int tailLength = GeometryHelper.Distance2D(Left + (Width / 2), Top + (Height / 2), targetGripper.Left, targetGripper.Top);
+			int tailWidth = (Math.Abs(Width) + Math.Abs(Height)) / 20;
+
+			GraphicsPath bubble = new GraphicsPath();
+			bubble.AddEllipse(0, 0, Math.Abs(rect.Width), Math.Abs(rect.Height));
+			bubble.CloseAllFigures();
+
+			GraphicsPath tail = new GraphicsPath();
+			tail.AddLine(-tailWidth, 0, tailWidth, 0);
+			tail.AddLine(tailWidth, 0, 0, -tailLength);
+			tail.CloseFigure();
+
+			GraphicsState state = graphics.Save();
+			// draw the tail border where the bubble is not visible
+			using (Region clipRegion = new Region(bubble)) {
+				clipRegion.Translate(Left, Top);
+				graphics.SetClip(clipRegion, CombineMode.Exclude);
+				graphics.TranslateTransform(Left + (Width / 2), Top + (Height / 2));
+				graphics.RotateTransform(tailAngle);
+				using (Pen pen = new Pen(lineColor, lineThickness)) {
+					graphics.DrawPath(pen, tail);
+				}
+			}
+			graphics.Restore(state);
+
+
+			if (Colors.IsVisible(fillColor)) {
+				//draw the bubbleshape
+				state = graphics.Save();
+				graphics.TranslateTransform(Left, Top);
+				using (Brush brush = new SolidBrush(fillColor)) {
+					graphics.FillPath(brush, bubble);
+				}
+				graphics.Restore(state);
+			}
+
+			if (lineVisible) {
+				//draw the bubble border
+				state = graphics.Save();
+				// Draw bubble where the Tail is not visible.
+				using (Region clipRegion = new Region(tail)) {
+					Matrix transformMatrix = new Matrix();
+					transformMatrix.Rotate(tailAngle);
+					clipRegion.Transform(transformMatrix);
+					clipRegion.Translate(Left + (Width / 2), Top + (Height / 2));
+					graphics.SetClip(clipRegion, CombineMode.Exclude);
+					graphics.TranslateTransform(Left, Top);
+					using (Pen pen = new Pen(lineColor, lineThickness)) {
+						graphics.DrawPath(pen, bubble);
+					}
+				}
+				graphics.Restore(state);
+			}
+
+			if (Colors.IsVisible(fillColor)) {
+				// Draw the tail border
+				state = graphics.Save();
+				graphics.TranslateTransform(Left + (Width / 2), Top + (Height / 2));
+				graphics.RotateTransform(tailAngle);
+				using (Brush brush = new SolidBrush(fillColor)) {
+					graphics.FillPath(brush, tail);
+				}
+				graphics.Restore(state);
+			}
+
+			// Draw the text
+			StringFormat format = new StringFormat();
+			format.Alignment = StringAlignment.Center;
+			format.LineAlignment = StringAlignment.Center;
+			base.DrawText(graphics, GuiRectangle.GetGuiRectangle(this.Left, this.Top, this.Width, this.Height), format);
+
+			// cleanup
+			bubble.Dispose();
+			tail.Dispose();
+		}
+		
+		public override bool Contains(int x, int y) {
+			double xDistanceFromCenter = x - (Left+Width/2);
+			double yDistanceFromCenter = y - (Top+Height/2);
+			// ellipse: x^2/a^2 + y^2/b^2 = 1
+			return Math.Pow(xDistanceFromCenter,2)/Math.Pow(Width/2,2) + Math.Pow(yDistanceFromCenter,2)/Math.Pow(Height/2,2) < 1;
+		}
+		
+		public override bool ClickableAt(int x, int y) {
+			Rectangle rect = GuiRectangle.GetGuiRectangle(this.Left, this.Top, this.Width, this.Height);
+			int lineThickness = GetFieldValueAsInt(FieldType.LINE_THICKNESS) + 10;
+			Color fillColor = GetFieldValueAsColor(FieldType.FILL_COLOR);
+
+			// If we clicked inside the rectangle and it's visible we are clickable at.
+			if (!Color.Transparent.Equals(fillColor)) {
+				if (Contains(x,y)) {
+					return true;
+				}
+			}
+
+			// check the rest of the lines
+			if (lineThickness > 0) {
+				using (Pen pen = new Pen(Color.White, lineThickness)) {
+					using (GraphicsPath path = new GraphicsPath()) {
+						path.AddEllipse(rect);
+						return path.IsOutlineVisible(x, y, pen);
+					}
+				}
+			} else {
+				return false;
+			}
+		}
+	}
+}
Index: Greenshot/Drawing/Surface.cs
===================================================================
--- Greenshot/Drawing/Surface.cs	(revision 2434)
+++ Greenshot/Drawing/Surface.cs	(working copy)
@@ -598,7 +598,7 @@
 					undrawnElement = new RectangleContainer(this);
 					break;
 				case DrawingModes.Ellipse:
-					undrawnElement = new EllipseContainer(this);
+					undrawnElement = new SpeechbubbleContainer(this);
 					break;
 				case DrawingModes.Text:
 					undrawnElement = new TextContainer(this);
Index: Greenshot/Drawing/TextContainer.cs
===================================================================
--- Greenshot/Drawing/TextContainer.cs	(revision 2434)
+++ Greenshot/Drawing/TextContainer.cs	(working copy)
@@ -308,15 +308,27 @@
 					}
 				}
 			}
+			DrawText(graphics, rect, null);
+		}
+
+		protected void DrawText(Graphics graphics, Rectangle drawingRectange, StringFormat stringFormat) {
+			UpdateFont();
 			Color lineColor = GetFieldValueAsColor(FieldType.LINE_COLOR);
-			Rectangle fontRect = rect;
+			Rectangle fontRect = drawingRectange;
+			Color fillColor = GetFieldValueAsColor(FieldType.FILL_COLOR);
+			int lineThickness = GetFieldValueAsInt(FieldType.LINE_THICKNESS);
+			int textOffset = (lineThickness > 0) ? (int)Math.Ceiling(lineThickness / 2d) : 0;
 			if (lineThickness > 0) {
 				graphics.SmoothingMode = SmoothingMode.HighSpeed;
 				fontRect.Inflate(-textOffset, -textOffset);
 			}
 			graphics.SmoothingMode = SmoothingMode.HighQuality;
 			using (Brush fontBrush = new SolidBrush(lineColor)) {
-				graphics.DrawString(text, font, fontBrush, fontRect);
+				if (stringFormat != null) {
+					graphics.DrawString(text, font, fontBrush, fontRect, stringFormat);
+				} else {
+					graphics.DrawString(text, font, fontBrush, fontRect);
+				}
 			}
 		}
 		
Index: Greenshot/Greenshot.csproj
===================================================================
--- Greenshot/Greenshot.csproj	(revision 2434)
+++ Greenshot/Greenshot.csproj	(working copy)
@@ -1,5 +1,5 @@
 ﻿<?xml version="1.0" encoding="utf-8"?>
-<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="3.5">
+<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
   <Import Project="..\CommonProject.properties" />
   <PropertyGroup>
     <ProjectGuid>{CD642BF4-D815-4D67-A0B5-C69F0B8231AF}</ProjectGuid>
@@ -104,6 +104,7 @@
     </Compile>
     <Compile Include="Drawing\HighlightContainer.cs" />
     <Compile Include="Drawing\IconContainer.cs" />
+    <Compile Include="Drawing\SpeechbubbleContainer.cs" />
     <Compile Include="Drawing\LineContainer.cs" />
     <Compile Include="Drawing\Fields\AbstractFieldHolder.cs" />
     <Compile Include="Drawing\Fields\Field.cs" />
