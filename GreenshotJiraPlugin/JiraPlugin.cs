/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using Greenshot.IniFile;
using GreenshotPlugin;
using System;
using GreenshotPlugin.Core;
using System.ComponentModel.Composition;
using log4net;
using System.Windows.Forms;
using System.IO;
using System.Reflection;
using Svg2Xaml;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using GreenshotPlugin.WPF;

namespace GreenshotJiraPlugin {
	/// <summary>
	/// This is the JiraPlugin base code
	/// </summary>
	[Export(typeof(IGreenshotPlugin))]
	[ExportMetadata("name", "Jira plug-in")]
	public class JiraPlugin : AbstractPlugin {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(JiraPlugin));
		private IGreenshotHost host;
		private JiraRest jiraConnector;
		private static readonly JiraConfiguration config = IniConfig.GetIniSection<JiraConfiguration>();
		static JiraPlugin() {
			using (Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("GreenshotJiraPlugin.JIRA.svg")) {
				JiraLogo = SvgReader.Load(stream);
			}
			// Generate "Image" with clip, this should be the icon part of the Jira logo.
			// Is a bit of a hack, maybe we can have 2 svg's...
			var bms = JiraLogo.ToBitmapSource();
			var iconSize = new Int32Rect(0, 0, ((int)bms.Width / 3) - 7, (int)bms.Height);
			var icon = new CroppedBitmap(bms, iconSize);
			JiraIcon = icon;
		}

		public static DrawingImage JiraLogo {
			get;
			private set;
		}

		public static BitmapSource JiraIcon {
			get;
			private set;
		}

		private static JiraPlugin instance;

		public static JiraPlugin Instance {
			get {
				return instance;
			}
		}

		public JiraRest JiraConnector {
			get {
				if (jiraConnector == null) {
					Login();
				}
				return jiraConnector;
			}
			private set {
				jiraConnector = value;
			}
		}

		public JiraPlugin() {
			instance = this;
		}
		
		public override IEnumerable<ILegacyDestination> Destinations() {
			yield return new JiraDestination(this);
		}

		/// <summary>
		/// Implementation of the IGreenshotPlugin.Initialize
		/// </summary>
		/// <param name="host">Use the IGreenshotPluginHost interface to register events</param>
		/// <param name="metadata">IDictionary<string, object></param>
		/// <returns>true if plugin is initialized, false if not (doesn't show)</returns>
		public override bool Initialize(IGreenshotHost pluginHost, IDictionary<string, object> metadata) {
			host = pluginHost;
			return true;
		}

		public void Login() {
			JiraRest jiraRest = null;
			try {
				// Get the system name, so the user knows where to login to
				string systemName = config.RestUrl.Replace(JiraConfiguration.DEFAULT_PREFIX, "");
				systemName = systemName.Replace(JiraConfiguration.DEFAULT_POSTFIX, "");
				CredentialsDialog dialog = new CredentialsDialog(systemName);
				dialog.Name = null;
				while (dialog.Show(dialog.Name) == DialogResult.OK) {
					jiraRest = new JiraRest(config.RestUrl, dialog.Name, dialog.Password);
					if (jiraRest.CanConnect) {
						if (dialog.SaveChecked) {
							dialog.Confirm(true);
						}
						JiraConnector = jiraRest;
						return;
					}
					try {
						dialog.Confirm(false);
					} catch (ApplicationException e) {
						// exception handling ...
						LOG.Error("Problem using the credentials dialog", e);
					}
					// For every windows version after XP show an incorrect password baloon
					dialog.IncorrectPassword = true;
					// Make sure the dialog is display, the password was false!
					dialog.AlwaysDisplay = true;
				}
			} catch (ApplicationException e) {
				// exception handling ...
				LOG.Error("Problem using the credentials dialog", e);
			}
		}
	}
}
