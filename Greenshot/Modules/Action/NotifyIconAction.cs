﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.ComponentModel.Composition;
using System.Windows;
using Hardcodet.Wpf.TaskbarNotification;
using GreenshotPlugin.Modules;
using Greenshot.Model;

namespace Greenshot.Services {
	/// <summary>
	/// This class will be automatically loaded and executed at startup
	/// </summary>
	[Export(typeof(IStartupAction))]
	[ExportMetadata(ModuleExtensions.ACTIVE, true)]
	[ExportMetadata(ModuleExtensions.PRIORITY, 2)]
	public class NotifyIconAction : IStartupAction {
		private static TaskbarIcon _taskbarIcon;
		public void Start() {
			_taskbarIcon = (TaskbarIcon)Application.Current.FindResource("GreenshotNotifyIcon");
			// Add the MainViewModel, so the NotifyIcon can bind to the context menu
			_taskbarIcon.DataContext = MainViewModel.Instance;
			_taskbarIcon.ShowBalloonTip("Greenshot", "Greenshot can be found here...", BalloonIcon.Info);
		}
	}
}
