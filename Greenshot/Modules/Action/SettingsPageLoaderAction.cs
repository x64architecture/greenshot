﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using log4net;
using GreenshotPlugin.Core.Settings;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;

namespace Greenshot.Services {
	/// <summary>
	/// This class will be automatically loaded and executed at startup
	/// </summary>
	[Export(typeof(IStartupAction))]
	[ExportMetadata(ModuleExtensions.ACTIVE, true)]
	[ExportMetadata(ModuleExtensions.PRIORITY, 2)]
	public class SettingsPageLoaderAction : IStartupAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(SettingsPageLoaderAction));

		/// <summary>
		/// Called when the application starts
		/// </summary>
		public async void Start() {
			await Task.Factory.StartNew(() => {
				foreach (var settingsPage in ModuleContainer.Instance.SettingsPages) {
					try {
						LOG.InfoFormat("Adding page to {0}", settingsPage.Metadata["path"]);
						SettingsWindow.RegisterSettingsPage((string)settingsPage.Metadata["path"], settingsPage.Value);
					} catch (Exception ex) {
						LOG.Error("Error added settings page: ", ex);
					}
				}
			}, CancellationToken.None, TaskCreationOptions.None, TaskScheduler.FromCurrentSynchronizationContext());
		}
	}
}
