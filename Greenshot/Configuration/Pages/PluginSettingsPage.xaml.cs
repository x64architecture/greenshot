﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
using System.ComponentModel.Composition;
using System;
using GreenshotPlugin;
using System.Reflection;
using System.IO;
using log4net;
using GreenshotPlugin.Modules;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the PluginSettingsPage.xaml
	/// </summary>
	[Export(typeof(SettingsPage))]
	[ExportMetadata(SettingsPage.PATH, "settings_plugins")]
	public partial class PluginSettingsPage : SettingsPage {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(PluginSettingsPage));

		public List<PluginInfo> Plugins {
			get {
				List<PluginInfo> pluginInfos = new List<PluginInfo>();
				foreach (var plugin in ModuleContainer.Instance.Plugins) {
					pluginInfos.Add(CreatePluginInfo(plugin));
				}
				return pluginInfos;
			}
		}
		public PluginSettingsPage() : base() {
			InitializeComponent();
		}


		/// <summary>
		/// A helper method to get a custom attribute from an assembly and invoke a lambda on it.
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="assembly"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		private static string GetAssemblyAttribute<T>(Assembly assembly, Func<T, string> value) where T : Attribute {
			T attribute = (T)Attribute.GetCustomAttribute(assembly, typeof(T));
			if (attribute != null) {
				return value.Invoke(attribute);
			}
			return null;
		}
		/// <summary>
		/// Create an object that has all information for showing a plugin
		/// </summary>
		/// <param name="plugin"></param>
		/// <returns>PluginInfo</returns>
		private PluginInfo CreatePluginInfo(Lazy<IGreenshotPlugin, IDictionary<string, object>> plugin) {
			PluginInfo info = new PluginInfo();
			try {
				string location = plugin.Value.GetType().Assembly.Location;
				info.Location = location;
				object name;
				if (!plugin.Metadata.TryGetValue("name", out name)) {
					name = GetAssemblyAttribute<AssemblyProductAttribute>(plugin.Value.GetType().Assembly, a => a.Product);
					if (string.IsNullOrEmpty((string)name)) {
						name = Path.GetFileNameWithoutExtension(location);
					}
				}
				info.Name = (string)name;
				object version;
				if (!plugin.Metadata.TryGetValue("version", out version)) {
					version = plugin.Value.GetType().Assembly.GetName().Version.ToString();
				}
				info.Version = (string)version;
				object createdBy;
				if (!plugin.Metadata.TryGetValue("createdBy", out createdBy)) {
					createdBy = GetAssemblyAttribute<AssemblyCompanyAttribute>(plugin.Value.GetType().Assembly, a => a.Company);
				}
				info.CreatedBy = (string)createdBy;
			} catch (Exception ex) {
				LOG.Error("Error displaying plugin: ", ex);
			}
			return info;
		}
	}
}
