﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using log4net;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Greenshot.Services {
	/// <summary>
	/// The GreenshotStartupActionTrigger calls all the IStartupAction & IShutdownAction actions
	/// </summary>
	[Export(typeof(IStartupTrigger))]
	[Export(typeof(IShutdownTrigger))]
	[ExportMetadata(ModuleExtensions.ACTIVE, true)]
	[ExportMetadata(ModuleExtensions.PRIORITY, -1)]
	public class GreenshotStartupActionTrigger : IStartupTrigger, IShutdownTrigger {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(GreenshotStartupActionTrigger));

		public void Startup() {
			LOG.Debug("Starting IStartupAction");
			// Startup all "Startup actions"
			foreach (var startupAction in ModuleContainer.Instance.Container.GetOrderedActiveExports<IStartupAction>()) {
				try {
					LOG.InfoFormat("Starting: {0}", startupAction.Value.GetType());
					startupAction.Value.Start();
				} catch (Exception ex) {
					if (startupAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing startupAction {0}: ", startupAction.Value.GetType()), ex);
					} else {
						LOG.Error("Exception instanciating startupAction: ", ex);
					}
				}
			}
		}

		public void Shutdown() {
			LOG.Debug("Shutting down IShutdownAction");
			// Startup all "Shutdown actions"
			foreach (var shutdownAction in ModuleContainer.Instance.Container.GetOrderedActiveExports<IShutdownAction>()) {
				try {
					LOG.InfoFormat("Shutting down: {0}", shutdownAction.Value.GetType());
					shutdownAction.Value.Shutdown();
				} catch (Exception ex) {
					if (shutdownAction.IsValueCreated) {
						LOG.Error(string.Format("Exception executing shutdownAction {0}: ", shutdownAction.Value.GetType()), ex);
					} else {
						LOG.Error("Exception instanciating shutdownAction: ", ex);
					}
				}
			}

		}
	}
}
