﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Runtime.InteropServices;
using Greenshot.IniFile;
using GreenshotPlugin.UnmanagedHelpers;
using GreenshotPlugin.Core;
using System.Windows;
using System.Windows.Interop;
using System.Windows.Media.Imaging;
using log4net;
using Screen = System.Windows.Forms.Screen;
using Region = System.Drawing.Region;
using System.Windows.Media;
using GreenshotPlugin.Modules;
using Point = System.Windows.Point;

namespace GreenshotPlugin.WPF.Capture {
	/// <summary>
	/// This is the screen source, and makes it possible to capture the screen
	/// </summary>
	public class MouseSource : ISource {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(WindowCapture));
		private static readonly CoreConfiguration Conf = IniConfig.GetIniSection<CoreConfiguration>();

		/// <summary>
		/// Capture the screen
		/// </summary>
		/// <returns></returns>
		public bool Import(CaptureContext captureContext) {
			captureContext.MouseCursor = Conf.CaptureMousepointer?CaptureCursor():null;
			return true;
		}

		public string Designation {
			get {
				return "MouseSource";
			}
		}

		public string Description {
			get {
				return Designation;
			}
		}

		public int Priority {
			get {
				return -1;
			}
		}

		public DependencyObject Visual {
			get {
				return null;
			}
		}

		public bool IsActive {
			get {
				return true;
			}
		}
	
		/// <summary>
		/// Used to cleanup the unmanged resource in the iconInfo for the CaptureCursor method
		/// </summary>
		/// <param name="hObject"></param>
		/// <returns></returns>
		[DllImport("gdi32", SetLastError = true)]
		private static extern bool DeleteObject(IntPtr hObject);

		/// <summary>
		/// Retrieves the cursor location safely, accounting for DPI settings in Vista/Windows 7.
		/// <returns>Point with cursor location, relative to the origin of the monitor setup (i.e. negative coordinates are
		/// possible in multiscreen setups)</returns>
		/// </summary>
		private static Point GetCursorLocation() {
			POINT cursorLocation;
			if (User32.GetPhysicalCursorPos(out cursorLocation)) {
				return new Point(cursorLocation.X, cursorLocation.Y);
			}
			Win32Error error = Win32.GetLastErrorCode();
			LOG.ErrorFormat("Error retrieving PhysicalCursorPos : {0}", Win32.GetMessage(error));
			return new Point(100, 100);
		}
		
		/// <summary>
		/// This method will capture the current Cursor by using User32 Code
		/// </summary>
		/// <returns>A IElement with the Mouse Cursor as Image in it.</returns>
		private static IElement<BitmapSource> CaptureCursor() {
			LOG.Debug("Capturing the mouse cursor.");
			IElement<BitmapSource> element = new BitmapSourceElement();

			CURSORINFO cursorInfo = new CURSORINFO(); 
			IconInfo iconInfo;
			cursorInfo.cbSize = Marshal.SizeOf(cursorInfo);
			if (User32.GetCursorInfo(out cursorInfo)) {
				if (cursorInfo.flags == User32.CURSOR_SHOWING) { 
					using (SafeIconHandle safeIcon = User32.CopyIcon(cursorInfo.hCursor)) {
						if (User32.GetIconInfo(safeIcon, out iconInfo)) {
							Point cursorLocation = GetCursorLocation();
							element.Content = Imaging.CreateBitmapSourceFromHIcon(safeIcon.DangerousGetHandle(), Int32Rect.Empty, BitmapSizeOptions.FromEmptyOptions());
							element.Bounds = new Rect(cursorLocation.X - iconInfo.xHotspot, cursorLocation.Y - iconInfo.yHotspot, element.Content.Width, element.Content.Height);

							if (iconInfo.hbmMask != IntPtr.Zero) {
								DeleteObject(iconInfo.hbmMask);
							}
							if (iconInfo.hbmColor != IntPtr.Zero) {
								DeleteObject(iconInfo.hbmColor);
							}
						}
					}
				}
			}
			return element;
		}
	}
}
