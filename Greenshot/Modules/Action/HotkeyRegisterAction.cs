﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.ComponentModel.Composition;
using GreenshotPlugin.Modules;
using log4net;
using GreenshotPlugin.Core;
using System.Threading.Tasks;
using System.Threading;
using GreenshotPlugin.Core.Settings;
using System.Text;
using Greenshot.IniFile;
using GreenshotPlugin;
using System.Windows;
using Greenshot.Configuration;
using Microsoft.Win32;
using System.IO;

namespace Greenshot.Services {
	/// <summary>
	/// This class will be automatically loaded and executed at startup
	/// </summary>
	[Export(typeof(IStartupAction))]
	[Export(typeof(IShutdownAction))]
	[ExportMetadata(ModuleExtensions.ACTIVE, true)]
	[ExportMetadata(ModuleExtensions.PRIORITY, 2)]
	public class HotkeyRegisterAction : System.Windows.Forms.NativeWindow, IStartupAction, IShutdownAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(HotkeyRegisterAction));
		private static readonly CoreConfiguration _conf = IniConfig.GetIniSection<CoreConfiguration>();

		public HotkeyRegisterAction() {
			// create the handle for the NativeWindow.
			this.CreateHandle(new System.Windows.Forms.CreateParams());
		}

		/// <summary>
		/// Called when the application starts
		/// </summary>
		public void Start() {
			// Make sure all hotkeys pass this window!
			SettingsHotkeyTextBox.RegisterHotkeyHWND(Handle);
			RegisterHotkeys(false);

		}

		/// <summary>
		/// Called when the application shuts down
		/// </summary>
		public void Shutdown() {
		}

		#region NativeWindow

		protected override void WndProc(ref System.Windows.Forms.Message m) {
			if (SettingsHotkeyTextBox.HandleMessages(ref m)) {
				return;
			}
			base.WndProc(ref m);
		}

		public void Dispose() {
			this.DestroyHandle();
		}

		#endregion

		/// <summary>
		/// Displays a dialog for the user to choose how to handle hotkey registration failures: 
		/// retry (allowing to shut down the conflicting application before),
		/// ignore (not registering the conflicting hotkey and resetting the respective config to "None", i.e. not trying to register it again on next startup)
		/// abort (do nothing about it)
		/// </summary>
		/// <param name="failedKeys">comma separated list of the hotkeys that could not be registered, for display in dialog text</param>
		/// <returns></returns>
		private static bool HandleFailedHotkeyRegistration(string failedKeys) {
			bool success = false;

			string text = Language.GetFormattedString(LangKey.warning_hotkeys, failedKeys);
			string caption = Language.GetString(LangKey.warning);
			var result = MessageBox.Show(text, caption, MessageBoxButton.OKCancel, MessageBoxImage.Exclamation);
			if (result == MessageBoxResult.OK) {
				LOG.DebugFormat("Re-trying to register hotkeys");
				SettingsHotkeyTextBox.UnregisterHotkeys();
				success = RegisterHotkeys(false);
			} else if (result == MessageBoxResult.Cancel) {
				LOG.DebugFormat("Ignoring failed hotkey registration");
				SettingsHotkeyTextBox.UnregisterHotkeys();
				success = RegisterHotkeys(true);
			}

			return success;
		}

		/// <summary>
		/// Registers all hotkeys as configured, displaying a dialog in case of hotkey conflicts with other tools.
		/// </summary>
		/// <param name="ignoreFailedRegistration">if true, a failed hotkey registration will not be reported to the user - the hotkey will simply not be registered</param>
		/// <returns>Whether the hotkeys could be registered to the users content. This also applies if conflicts arise and the user decides to ignore these (i.e. not to register the conflicting hotkey).</returns>
		private static bool RegisterHotkeys(bool ignoreFailedRegistration) {
			bool success = true;
			StringBuilder failedKeys = new StringBuilder();

			if (!RegisterWrapper(failedKeys, "CaptureRegion", "RegionHotkey", CaptureRegion, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureWindow", "WindowHotkey", CaptureWindow, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureFullScreen", "FullscreenHotkey", CaptureFullScreen, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureLastRegion", "LastregionHotkey", CaptureLastRegion, ignoreFailedRegistration)) {
				success = false;
			}
			if (_conf.IECapture) {
				if (!RegisterWrapper(failedKeys, "CaptureIE", "IEHotkey", CaptureIE, ignoreFailedRegistration)) {
					success = false;
				}
			}

			if (!success) {
				if (!ignoreFailedRegistration) {
					success = HandleFailedHotkeyRegistration(failedKeys.ToString());
				} else {
					// if failures have been ignored, the config has probably been updated
					if (_conf.IsDirty)
						IniConfig.Save();
				}
			}
			return success || ignoreFailedRegistration;
		}

		/// <summary>
		/// Helper method to cleanly register a hotkey
		/// </summary>
		/// <param name="failedKeys"></param>
		/// <param name="functionName"></param>
		/// <param name="hotkeyString"></param>
		/// <param name="handler"></param>
		/// <returns></returns>
		private static bool RegisterHotkey(StringBuilder failedKeys, string functionName, string hotkeyString, HotKeyHandler handler) {
			if (SettingsHotkeyTextBox.RegisterHotKey(hotkeyString, handler) < 0) {
				LOG.DebugFormat("Failed to register {0} to hotkey: {1}", functionName, hotkeyString);
				if (failedKeys.Length > 0) {
					failedKeys.Append(", ");
				}
				failedKeys.Append(hotkeyString);
				return false;
			}
			LOG.DebugFormat("Registered {0} to hotkey: {1}", functionName, hotkeyString);
			return true;
		}

		private static bool RegisterWrapper(StringBuilder failedKeys, string functionName, string configurationKey, HotKeyHandler handler, bool ignoreFailedRegistration) {
			IniValue hotkeyValue = _conf.Values[configurationKey];
			try {
				bool success = RegisterHotkey(failedKeys, functionName, hotkeyValue.Value.ToString(), handler);
				if (!success && ignoreFailedRegistration) {
					LOG.DebugFormat("Ignoring failed hotkey registration, resetting to 'None'.", functionName, hotkeyValue);
					_conf.Values[configurationKey].Value = System.Windows.Forms.Keys.None.ToString();
					_conf.IsDirty = true;
				}
				return success;
			} catch (Exception ex) {
				LOG.Warn(ex);
				LOG.WarnFormat("Restoring default hotkey for {0}, stored under {1} from '{2}' to '{3}'", functionName, configurationKey, hotkeyValue.Value, hotkeyValue.Attributes.DefaultValue);
				// when getting an exception the key wasn't found: reset the hotkey value
				hotkeyValue.UseValueOrDefault(null);
				hotkeyValue.ContainingIniSection.IsDirty = true;
				return RegisterHotkey(failedKeys, functionName, hotkeyValue.Value.ToString(), handler);
			}
		}

		public static void CaptureRegion() {
			CaptureHelper.CaptureRegion(true);
		}

		public static void CaptureClipboard() {
			CaptureHelper.CaptureClipboard();
		}

		public static void CaptureFile() {
			var openFileDialog = new OpenFileDialog {
				Filter = @"Image files (*.greenshot, *.png, *.jpg, *.gif, *.bmp, *.ico, *.tiff, *.wmf)|*.greenshot; *.png; *.jpg; *.jpeg; *.gif; *.bmp; *.ico; *.tiff; *.tif; *.wmf"
			};
			var result = openFileDialog.ShowDialog();
			if (result.HasValue && result.Value) {
				if (File.Exists(openFileDialog.FileName)) {
					CaptureHelper.CaptureFile(openFileDialog.FileName);
				}
			}
		}

		public static void CaptureFullScreen() {
			CaptureHelper.CaptureFullscreen(true, _conf.ScreenCaptureMode);
		}

		public static void CaptureLastRegion() {
			CaptureHelper.CaptureLastRegion(true);
		}

		public static void CaptureIE() {
			if (_conf.IECapture) {
				CaptureHelper.CaptureIE(true, null);
			}
		}

		public static void CaptureWindow() {
			if (_conf.CaptureWindowsInteractive) {
				CaptureHelper.CaptureWindowInteractive(true);
			} else {
				CaptureHelper.CaptureWindow(true);
			}
		}
	}
}
