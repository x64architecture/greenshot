/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using GreenshotPlugin.Drawing;

namespace GreenshotEditor.Drawing {
	/// <summary>
	/// Represents a rectangular shape on the Surface
	/// </summary>
	[Serializable()] 
	public class RectangleContainer : DrawableContainer {
		protected int lineThickness = 2;
		[Field(FieldTypes.LINE_THICKNESS)]
		public int LineThickness {
			get {
				return lineThickness;
			}
			set {
				lineThickness = value;
				OnFieldPropertyChanged(FieldTypes.LINE_THICKNESS);
			}
		}

		protected Color lineColor = Color.Red;
		[Field(FieldTypes.LINE_COLOR)]
		public Color LineColor {
			get {
				return lineColor;
			}
			set {
				lineColor = value;
				OnFieldPropertyChanged(FieldTypes.LINE_COLOR);
			}
		}

		protected Color fillColor = Color.Transparent;
		[Field(FieldTypes.FILL_COLOR)]
		public Color FillColor {
			get {
				return fillColor;
			}
			set {
				fillColor = value;
				OnFieldPropertyChanged(FieldTypes.FILL_COLOR);
			}
		}

		protected bool shadow = true;
		[Field(FieldTypes.SHADOW)]
		public bool Shadow {
			get {
				return shadow;
			}
			set {
				shadow = value;
				OnFieldPropertyChanged(FieldTypes.SHADOW);
			}
		}

		public RectangleContainer(Surface parent) : base(parent) {
		}
		
		
		public override void Draw(Graphics graphics, RenderMode rm) {
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.CompositingQuality = CompositingQuality.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.None;

			bool lineVisible = (lineThickness > 0 && Colors.IsVisible(lineColor));
			if (shadow && (lineVisible || Colors.IsVisible(fillColor))) {
				//draw shadow first
				int basealpha = 100;
				int alpha = basealpha;
				int steps = 5;
				int currentStep = lineVisible ? 1 : 0;
				while (currentStep <= steps) {
					using (Pen shadowPen = new Pen(Color.FromArgb(alpha, 100, 100, 100))) {
						shadowPen.Width = lineVisible ? lineThickness : 1;
						Rectangle shadowRect = GuiRectangle.GetGuiRectangle(
							Left + currentStep,
							Top + currentStep,
							Width,
							Height);
						graphics.DrawRectangle(shadowPen, shadowRect);
						currentStep++;
						alpha = alpha - (basealpha / steps);
					}
				}
			}
			
			Rectangle rect = GuiRectangle.GetGuiRectangle(Left, Top, Width, Height);

			if (Colors.IsVisible(fillColor)) {
				using (Brush brush = new SolidBrush(fillColor)) {
					graphics.FillRectangle(brush, rect);
				}
			}

			graphics.SmoothingMode = SmoothingMode.HighSpeed;
			if (lineVisible) {
				using (Pen pen = new Pen(lineColor, lineThickness)) {
					graphics.DrawRectangle(pen, rect);
				}
			}
		}
		
		public override bool ClickableAt(int x, int y) {
			Rectangle rect = GuiRectangle.GetGuiRectangle(Left, Top, Width, Height);
			int lineWidth = lineThickness + 10;

			// If we clicked inside the rectangle and it's visible we are clickable at.
			if (!Color.Transparent.Equals(fillColor)) {
				if (rect.Contains(x,y)) {
					return true;
				}
			}

			// check the rest of the lines
			if (lineThickness > 0) {
				using (Pen pen = new Pen(Color.White, lineWidth)) {
					using (GraphicsPath path = new GraphicsPath()) {
						path.AddRectangle(rect);
						return path.IsOutlineVisible(x, y, pen);
					}
				}
			} else {
				return false;
			}
		}
	}
}
