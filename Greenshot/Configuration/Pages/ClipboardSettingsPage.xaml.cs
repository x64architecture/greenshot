﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using GreenshotPlugin.Core.Settings;
using System.ComponentModel.Composition;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the ClipboardSettingsPage.xaml
	/// </summary>
	[Export(typeof(SettingsPage))]
	[ExportMetadata(SettingsPage.PATH, "settings_destination,settings_destination_clipboard")]
	public partial class ClipboardSettingsPage : SettingsPage {
		public ClipboardSettingsPage() : base() {
			InitializeComponent();
		}
	}
}
