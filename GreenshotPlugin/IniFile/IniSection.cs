﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
using GreenshotPlugin.Core;
using System.Text;
using System.Security.Cryptography;
using log4net;

namespace Greenshot.IniFile {
	/// <summary>
	/// Base class for all IniSections
	/// </summary>
	[Serializable]
	public abstract class IniSection {
		protected static ILog LOG = LogManager.GetLogger(typeof(IniSection));

		[NonSerialized]
		private readonly IDictionary<string, IniValue> _values = new Dictionary<string, IniValue>();
		[NonSerialized]
		private IniSectionAttribute _iniSectionAttribute;
		public IniSectionAttribute IniSectionAttribute {
			get {
				if (_iniSectionAttribute == null) {
					_iniSectionAttribute = GetIniSectionAttribute(GetType());
				}
				return _iniSectionAttribute;
			}
		}

		/// <summary>
		/// Get the dictionary with all the IniValues
		/// </summary>
		public IDictionary<string, IniValue> Values {
			get {
				return _values;
			}
		}

		/// <summary>
		/// Flag to specify if values have been changed
		/// </summary>
		public bool IsDirty = false;

		/// <summary>
		/// Supply values we can't put as defaults
		/// </summary>
		/// <param name="property">The property to return a default for</param>
		/// <returns>object with the default value for the supplied property</returns>
		public virtual object GetDefault(string property) {
			return null;
		}

		/// <summary>
		/// This method will be called before converting the property, making to possible to correct a certain value
		/// Can be used when migration is needed
		/// </summary>
		/// <param name="propertyName">The name of the property</param>
		/// <param name="propertyValue">The string value of the property</param>
		/// <returns>string with the propertyValue, modified or not...</returns>
		public virtual string PreCheckValue(string propertyName, string propertyValue) {
			return propertyValue;
		}

		/// <summary>
		/// This method will be called after reading the configuration, so eventually some corrections can be made
		/// </summary>
		public virtual void AfterLoad() {
		}

		/// <summary>
		/// This will be called before saving the Section...
		/// </summary>
		public virtual void BeforeSave() {
		}

		/// <summary>
		/// This will be called after saving the Section...
		/// </summary>
		public virtual void AfterSave() {
		}

		/// <summary>
		/// Helper method to get the IniSectionAttribute of a type
		/// </summary>
		/// <param name="iniSectionType"></param>
		/// <returns></returns>
		public static IniSectionAttribute GetIniSectionAttribute(Type iniSectionType) {
			Attribute[] classAttributes = Attribute.GetCustomAttributes(iniSectionType);
			foreach (Attribute attribute in classAttributes) {
				if (attribute is IniSectionAttribute) {
					return (IniSectionAttribute)attribute;
				}
			}
			return null;
		}

		/// <summary>
		/// Fill the section with the supplied properties
		/// </summary>
		/// <param name="properties"></param>
		public void Fill(Dictionary<string, string> properties) {
			Type iniSectionType = GetType();

			// Iterate over the members and create IniValueContainers
			foreach (FieldInfo fieldInfo in iniSectionType.GetFields()) {
				if (Attribute.IsDefined(fieldInfo, typeof(IniPropertyAttribute))) {
					IniPropertyAttribute iniPropertyAttribute = (IniPropertyAttribute)fieldInfo.GetCustomAttributes(typeof(IniPropertyAttribute), false)[0];
					if (!Values.ContainsKey(fieldInfo.Name)) {
						Values.Add(fieldInfo.Name, new IniValue(this, fieldInfo, iniPropertyAttribute));
					}
				}
			}

			foreach (PropertyInfo propertyInfo in iniSectionType.GetProperties()) {
				if (Attribute.IsDefined(propertyInfo, typeof(IniPropertyAttribute))) {
					if (!Values.ContainsKey(propertyInfo.Name)) {
						IniPropertyAttribute iniPropertyAttribute = (IniPropertyAttribute)propertyInfo.GetCustomAttributes(typeof(IniPropertyAttribute), false)[0];
						iniPropertyAttribute.Name = propertyInfo.Name;
						Values.Add(propertyInfo.Name, new IniValue(this, propertyInfo, iniPropertyAttribute));
					}
				}
			}

			foreach (string fieldName in Values.Keys) {
				IniValue iniValue = Values[fieldName];
				try {
					iniValue.SetValueFromProperties(properties);
					if (iniValue.Attributes.Encrypted) {
						string stringValue = iniValue.Value as string;
						if (stringValue != null && stringValue.Length > 2) {
							iniValue.Value = Decrypt(stringValue);
						}
					}
				} catch (Exception ex) {
					LOG.Error(ex);
				}
			}
			AfterLoad();
		}

		/// <summary>
		/// Write the section to the writer
		/// </summary>
		/// <param name="writer"></param>
		/// <param name="onlyProperties"></param>
		public void Write(TextWriter writer, bool onlyProperties) {
			if (IniSectionAttribute == null) {
				throw new ArgumentException("Section didn't implement the IniSectionAttribute");
			}
			BeforeSave();
			try {

				if (!onlyProperties) {
					writer.WriteLine("; {0}", IniSectionAttribute.Description);
				}
				writer.WriteLine("[{0}]", IniSectionAttribute.Name);

				foreach (IniValue value in Values.Values) {
					if (value.IsFixed) {
						// No reason to write, it's fixed!
						continue;
					}
					if (value.Attributes.Encrypted) {
						string stringValue = value.Value as string;
						if (stringValue != null && stringValue.Length > 2) {
							value.Value = Encrypt(stringValue);
						}
					}
					// Write the value
					value.Write(writer, onlyProperties);
					if (value.Attributes.Encrypted) {
						string stringValue = value.Value as string;
						if (stringValue != null && stringValue.Length > 2) {
							value.Value = Decrypt(stringValue);
						}
					}
				}
			} finally {
				AfterSave();
			}
		}

		private const string RGBIV = "dlgjowejgogkklwj";
		private const string KEY = "lsjvkwhvwujkagfauguwcsjgu2wueuff";

		/// <summary>
		/// A simply rijndael aes encryption, can be used to store passwords
		/// </summary>
		/// <param name="clearText">the string to call upon</param>
		/// <returns>an encryped string in base64 form</returns>
		public static string Encrypt(string clearText) {
			string returnValue = clearText;
			try {
				byte[] clearTextBytes = Encoding.ASCII.GetBytes(clearText);
				SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();

				using (MemoryStream ms = new MemoryStream()) {
					byte[] rgbIV = Encoding.ASCII.GetBytes(RGBIV);
					byte[] key = Encoding.ASCII.GetBytes(KEY);
					CryptoStream cs = new CryptoStream(ms, rijn.CreateEncryptor(key, rgbIV), CryptoStreamMode.Write);

					cs.Write(clearTextBytes, 0, clearTextBytes.Length);

					cs.Close();
					returnValue = Convert.ToBase64String(ms.ToArray());
				}
			} catch (Exception ex) {
				LOG.ErrorFormat("Error encrypting, error: ", ex.Message);
			}
			return returnValue;
		}

		/// <summary>
		/// A simply rijndael aes decryption, can be used to store passwords
		/// </summary>
		/// <param name="encryptedText">a base64 encoded rijndael encrypted string</param>
		/// <returns>Decrypeted text</returns>
		public static string Decrypt(string encryptedText) {
			string returnValue = encryptedText;
			try {
				byte[] encryptedTextBytes = Convert.FromBase64String(encryptedText);
				using (MemoryStream ms = new MemoryStream()) {
					SymmetricAlgorithm rijn = SymmetricAlgorithm.Create();


					byte[] rgbIV = Encoding.ASCII.GetBytes(RGBIV);
					byte[] key = Encoding.ASCII.GetBytes(KEY);

					CryptoStream cs = new CryptoStream(ms, rijn.CreateDecryptor(key, rgbIV),
					CryptoStreamMode.Write);

					cs.Write(encryptedTextBytes, 0, encryptedTextBytes.Length);

					cs.Close();
					returnValue = Encoding.ASCII.GetString(ms.ToArray());
				}
			} catch (Exception ex) {
				LOG.ErrorFormat("Error decrypting {0}, error: ", encryptedText, ex.Message);
			}

			return returnValue;
		}
	}
}
