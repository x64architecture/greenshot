﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System.Collections.Generic;
using System.Drawing;
using System.IO;

using GreenshotPlugin.Core;
using GreenshotPlugin;
using Greenshot.Interop.Office;
using System.Text.RegularExpressions;

namespace GreenshotOfficePlugin {
	/// <summary>
	/// Description of PowerpointDestination.
	/// </summary>
	public class ExcelDestination : AbstractDestination {
		private static readonly string ExePath;
		private static readonly Image ApplicationIcon;
		private static readonly Image WorkbookIcon;
		private readonly string _workbookName;

		static ExcelDestination() {
			ExePath = PluginUtils.GetExePath("EXCEL.EXE");
			if (ExePath != null && File.Exists(ExePath)) {
				ApplicationIcon = PluginUtils.GetExeIcon(ExePath, 0);
				WorkbookIcon = PluginUtils.GetExeIcon(ExePath, 1);
				WindowDetails.AddProcessToExcludeFromFreeze("excel");
			} else {
				ExePath = null;
			}
		}

		public ExcelDestination() {
		}

		public ExcelDestination(string workbookName) {
			_workbookName = workbookName;
		}

		public override string Designation {
			get {
				return "Excel";
			}
		}

		public override string Description {
			get {
				if (_workbookName == null) {
					return "Microsoft Excel";
				}
				return _workbookName;
			}
		}

		public override int Priority {
			get {
				return 5;
			}
		}
		
		public override bool IsDynamic {
			get {
				return true;
			}
		}

		public override bool IsActive {
			get {
				return base.IsActive && ExePath != null;
			}
		}

		public override Image DisplayIcon {
			get {
				if (!string.IsNullOrEmpty(_workbookName)) {
					return WorkbookIcon;
				}
				return ApplicationIcon;
			}
		}

		public override IEnumerable<ILegacyDestination> DynamicDestinations() {
			foreach (string workbookName in ExcelExporter.GetWorkbooks()) {
				yield return new ExcelDestination(workbookName);
			}
		}

		public override ExportInformation ExportCapture(bool manuallyInitiated, ISurface surface, ICaptureDetails captureDetails) {
			ExportInformation exportInformation = new ExportInformation(Designation, Description);
			bool createdFile = false;
			string imageFile = captureDetails.Filename;
			if (imageFile == null || surface.Modified || !Regex.IsMatch(imageFile, @".*(\.png|\.gif|\.jpg|\.jpeg|\.tiff|\.bmp)$")) {
				imageFile = ImageOutput.SaveNamedTmpFile(surface, captureDetails, new SurfaceOutputSettings().PreventGreenshotFormat());
				createdFile = true;
			}
			if (_workbookName != null) {
				ExcelExporter.InsertIntoExistingWorkbook(_workbookName, imageFile, surface.Image.Size);
			} else {
				ExcelExporter.InsertIntoNewWorkbook(imageFile, surface.Image.Size);
			}
			exportInformation.ExportMade = true;
			ProcessExport(exportInformation, surface);
			// Cleanup imageFile if we created it here, so less tmp-files are generated and left
			if (createdFile && File.Exists(imageFile)) {
				File.Delete(imageFile);
			}
			return exportInformation;
		}
	}
}
