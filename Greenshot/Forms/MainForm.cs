/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Linq;

using Greenshot.Configuration;
using Greenshot.Forms;
using Greenshot.Helpers;
using GreenshotPlugin;
using GreenshotPlugin.UnmanagedHelpers;
using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;
using Greenshot.IniFile;
using System.ServiceModel;
using GreenshotPlugin.Core.Capturing;
using GreenshotPlugin.Core.Settings;
using GreenshotPlugin.WPF;
using System.Windows.Forms.Integration;
using Greenshot.Windows;
using log4net;
using Timer = System.Timers.Timer;
using GreenshotEditor.Configuration.Pages;
using System.Windows.Interop;
using Greenshot.Services;

namespace Greenshot {
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public partial class MainForm : BaseForm, IGreenshotContract, IGreenshotMain {
		private static readonly ILog _log = LogManager.GetLogger(typeof(MainForm));
		private static readonly CoreConfiguration _conf = IniConfig.GetIniSection<CoreConfiguration>();

		public static void Start(string[] args) {
			// Log the startup
			_log.Info("Starting: " + EnvironmentInfo.EnvironmentToString(false));

			// if language is not set, show language dialog
			if(string.IsNullOrEmpty(_conf.Language)) {
				LanguageDialog languageDialog = LanguageDialog.GetInstance();
				languageDialog.ShowDialog();
				_conf.Language = languageDialog.SelectedLanguage;
				IniConfig.Save();
			}

			// Check if it's the first time launch?

			MainForm mainForm = new MainForm();

			Application.Run(mainForm);
		}

		private static MainForm _instance;
		public static MainForm Instance {
			get {
				return _instance;
			}
		}
		
		// Thumbnail preview
		private ThumbnailForm _thumbnailForm;
		// Make sure we have only one settings form
		private SettingsWindow _settingsWindow;
		// Make sure we have only one about form
		private WindowInteropHelper _aboutWindow;
		// Timer for the double click test
		private readonly Timer _doubleClickTimer = new Timer();

		public NotifyIcon NotifyIcon {
			get {
				return notifyIcon;				
			}
		}

		public MainForm() {
			_instance = this;
			PluginHelper.Instance.GreenshotMain = this;

			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			try {
				InitializeComponent();
			} catch (ArgumentException ex) {
				// Added for Bug #1420, this doesn't solve the issue but maybe the user can do something with it.
				ex.Data.Add("more information here", "http://support.microsoft.com/kb/943140");
				throw;
			}
			notifyIcon.Icon = GreenshotResources.GetGreenshotIcon();
			Icon = GreenshotResources.GetGreenshotIcon();

			// Disable access to the settings, for feature #3521446
			contextmenu_settings.Visible = !_conf.DisableSettings;

			UpdateUI();

			// This forces the registration of all destinations inside Greenshot itself.
			DestinationHelper.GetAllDestinations();
			// This forces the registration of all processors inside Greenshot itself.
			ProcessorHelper.GetAllProcessors();
			
			// Check destinations, remove all that don't exist
			foreach(string destination in _conf.OutputDestinations.ToArray()) {
				if (DestinationHelper.GetDestination(destination) == null) {
					_conf.OutputDestinations.Remove(destination);
				}
			}

			// we should have at least one!
			if (_conf.OutputDestinations.Count == 0) {
				_conf.OutputDestinations.Add(KnownDesignations.Editor.ToString());
			}
			if (_conf.DisableQuickSettings) {
				contextmenu_quicksettings.Visible = false;
			} else {
				// Do after all plugins & finding the destination, otherwise they are missing!
				InitializeQuickSettingsMenu();
			}
			SoundHelper.Initialize();

			// Set the Greenshot icon visibility depending on the configuration. (Added for feature #3521446)
			// Setting it to true this late prevents Problems with the context menu
			notifyIcon.Visible = !_conf.HideTrayicon;

			// Make sure we never capture the mainform
			WindowDetails.RegisterIgnoreHandle(Handle);

			if (_conf.IsFirstLaunch) {
				_conf.IsFirstLaunch = false;
				IniConfig.Save();
				FirstLaunch();
			}
		}

		protected override void OnLoad(EventArgs e) {
			base.OnLoad(e);
			// enable the WinEventHook code
			WinEventHook.Instance.Hook();
		}

		private void FirstLaunch() {
			_log.Info("FirstLaunch: Created new configuration, showing balloon.");

			try {
				EventHandler balloonTipClickedHandler = null;
				EventHandler balloonTipClosedHandler = null;
				balloonTipClosedHandler = delegate {
					notifyIcon.BalloonTipClicked -= balloonTipClickedHandler;
					notifyIcon.BalloonTipClosed -= balloonTipClosedHandler;
				};

				balloonTipClickedHandler = delegate {
					ShowSetting();
					notifyIcon.BalloonTipClicked -= balloonTipClickedHandler;
					notifyIcon.BalloonTipClosed -= balloonTipClosedHandler;
				};
				notifyIcon.BalloonTipClicked += balloonTipClickedHandler;
				notifyIcon.BalloonTipClosed += balloonTipClosedHandler;
				notifyIcon.ShowBalloonTip(2000, "Greenshot", Language.GetFormattedString(LangKey.tooltip_firststart, SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.RegionHotkey)), ToolTipIcon.Info);
			} catch (Exception ex) {
				_log.Warn("Error in FirstLaunch: ", ex);
			}
		}

		public void ReloadConfig() {
			_log.Info("Reload requested");
			try {
				IniConfig.Reload();
				Language.CurrentLanguage = null; // Reload
				Invoke((MethodInvoker) delegate {
					// Even update language when needed
					UpdateUI();
					// Update the hotkey
					// Make sure the current hotkeys are disabled
					SettingsHotkeyTextBox.UnregisterHotkeys();
					RegisterHotkeys();
				});
			} catch (Exception ex) {
				_log.Warn("Error in ReloadConfig: ", ex);
			}
		}

		public void OpenFile(string filename) {
			_log.InfoFormat("Open file requested: {0}", filename);
			if (File.Exists(filename)) {
				BeginInvoke((MethodInvoker)(() => CaptureHelper.CaptureFile(filename)));
			} else {
				_log.Warn("No such file: " + filename);
			}
		}

		public ContextMenuStrip MainMenu {
			get {
				return contextMenu;
			}
		}

		#region hotkeys
		protected override void WndProc(ref Message m) {
			if (SettingsHotkeyTextBox.HandleMessages(ref m)) {
				return;
			}
			base.WndProc(ref m);
		}

		/// <summary>
		/// Helper method to cleanly register a hotkey
		/// </summary>
		/// <param name="failedKeys"></param>
		/// <param name="functionName"></param>
		/// <param name="hotkeyString"></param>
		/// <param name="handler"></param>
		/// <returns></returns>
		private static bool RegisterHotkey(StringBuilder failedKeys, string functionName, string hotkeyString, HotKeyHandler handler) {
			if (SettingsHotkeyTextBox.RegisterHotKey(hotkeyString, handler) < 0) {
				_log.DebugFormat("Failed to register {0} to hotkey: {1}", functionName, hotkeyString);
				if (failedKeys.Length > 0) {
					failedKeys.Append(", ");
				}
				failedKeys.Append(hotkeyString);
				return false;
			}
			_log.DebugFormat("Registered {0} to hotkey: {1}", functionName, hotkeyString);
			return true;
		}

		private static bool RegisterWrapper(StringBuilder failedKeys, string functionName, string configurationKey, HotKeyHandler handler, bool ignoreFailedRegistration) {
			IniValue hotkeyValue = _conf.Values[configurationKey];
			try {
				bool success = RegisterHotkey(failedKeys, functionName, hotkeyValue.Value.ToString(), handler);
				if (!success && ignoreFailedRegistration) {
					_log.DebugFormat("Ignoring failed hotkey registration, resetting to 'None'.", functionName, hotkeyValue);
					_conf.Values[configurationKey].Value = Keys.None.ToString();
					_conf.IsDirty = true;
				}
				return success;
			} catch (Exception ex) {
				_log.Warn(ex);
				_log.WarnFormat("Restoring default hotkey for {0}, stored under {1} from '{2}' to '{3}'", functionName, configurationKey, hotkeyValue.Value, hotkeyValue.Attributes.DefaultValue);
				// when getting an exception the key wasn't found: reset the hotkey value
				hotkeyValue.UseValueOrDefault(null);
				hotkeyValue.ContainingIniSection.IsDirty = true;
				return RegisterHotkey(failedKeys, functionName, hotkeyValue.Value.ToString(), handler);
			}
		}

		/// <summary>
		/// Registers all hotkeys as configured, displaying a dialog in case of hotkey conflicts with other tools.
		/// </summary>
		/// <returns>Whether the hotkeys could be registered to the users content. This also applies if conflicts arise and the user decides to ignore these (i.e. not to register the conflicting hotkey).</returns>
		public static bool RegisterHotkeys() {
			return RegisterHotkeys(false);
		}

		/// <summary>
		/// Registers all hotkeys as configured, displaying a dialog in case of hotkey conflicts with other tools.
		/// </summary>
		/// <param name="ignoreFailedRegistration">if true, a failed hotkey registration will not be reported to the user - the hotkey will simply not be registered</param>
		/// <returns>Whether the hotkeys could be registered to the users content. This also applies if conflicts arise and the user decides to ignore these (i.e. not to register the conflicting hotkey).</returns>
		private static bool RegisterHotkeys(bool ignoreFailedRegistration) {
			if (_instance == null) {
				return false;
			}
			bool success = true;
			StringBuilder failedKeys = new StringBuilder();
            
			if (!RegisterWrapper(failedKeys, "CaptureRegion", "RegionHotkey", _instance.CaptureRegion, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureWindow", "WindowHotkey", _instance.CaptureWindow, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureFullScreen", "FullscreenHotkey", _instance.CaptureFullScreen, ignoreFailedRegistration)) {
				success = false;
			}
			if (!RegisterWrapper(failedKeys, "CaptureLastRegion", "LastregionHotkey", _instance.CaptureLastRegion, ignoreFailedRegistration)) {
				success = false;
			}
			if (_conf.IECapture) {
				if (!RegisterWrapper(failedKeys, "CaptureIE", "IEHotkey", _instance.CaptureIE, ignoreFailedRegistration)) {
					success = false;
				}
			}

			if (!success) {
				if (!ignoreFailedRegistration) {
					success = HandleFailedHotkeyRegistration(failedKeys.ToString());
				} else {
					// if failures have been ignored, the config has probably been updated
					if (_conf.IsDirty) IniConfig.Save();
				}
			}
			return success || ignoreFailedRegistration;
		}

		/// <summary>
		/// Displays a dialog for the user to choose how to handle hotkey registration failures: 
		/// retry (allowing to shut down the conflicting application before),
		/// ignore (not registering the conflicting hotkey and resetting the respective config to "None", i.e. not trying to register it again on next startup)
		/// abort (do nothing about it)
		/// </summary>
		/// <param name="failedKeys">comma separated list of the hotkeys that could not be registered, for display in dialog text</param>
		/// <returns></returns>
		private static bool HandleFailedHotkeyRegistration(string failedKeys) {
			bool success = false;
			
			DialogResult dr = MessageBox.Show(Instance, Language.GetFormattedString(LangKey.warning_hotkeys, failedKeys), Language.GetString(LangKey.warning), MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Exclamation);
			if (dr == DialogResult.Retry) {
				_log.DebugFormat("Re-trying to register hotkeys");
				SettingsHotkeyTextBox.UnregisterHotkeys();
				success = RegisterHotkeys(false);
			} else if (dr == DialogResult.Ignore)  {
				_log.DebugFormat("Ignoring failed hotkey registration");
				SettingsHotkeyTextBox.UnregisterHotkeys();
				success = RegisterHotkeys(true);
			}
			
			return success;
		}
		#endregion
		
		public void UpdateUI() {
			// As the form is never loaded, call ApplyLanguage ourselves
			ApplyLanguage();

			// Show hotkeys in Contextmenu
			contextmenu_capturearea.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.RegionHotkey);
			contextmenu_capturelastregion.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.LastregionHotkey);
			contextmenu_capturewindow.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.WindowHotkey);
			contextmenu_capturefullscreen.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.FullscreenHotkey);
			contextmenu_captureie.ShortcutKeyDisplayString = SettingsHotkeyTextBox.GetLocalizedHotkeyStringFromString(_conf.IEHotkey);
		}		
		
		#region mainform events
		void MainFormFormClosing(object sender, FormClosingEventArgs e) {
			_log.DebugFormat("Mainform closing, reason: {0}", e.CloseReason);
			_instance = null;
			Exit();
		}

		void MainFormActivated(object sender, EventArgs e) {
			Hide();
			ShowInTaskbar = false;
		}
		#endregion

		#region key handlers
		public void CaptureRegion() {
			CaptureHelper.CaptureRegion(true);
		}

		public void CaptureClipboard() {
			CaptureHelper.CaptureClipboard();
		}

		public void CaptureFile() {
			var openFileDialog = new OpenFileDialog {
				Filter = @"Image files (*.greenshot, *.png, *.jpg, *.gif, *.bmp, *.ico, *.tiff, *.wmf)|*.greenshot; *.png; *.jpg; *.jpeg; *.gif; *.bmp; *.ico; *.tiff; *.tif; *.wmf"
			};
			if (openFileDialog.ShowDialog() == DialogResult.OK) {
				if (File.Exists(openFileDialog.FileName)) {
					CaptureHelper.CaptureFile(openFileDialog.FileName);
				}
			}
		}

		public void CaptureFullScreen() {
			CaptureHelper.CaptureFullscreen(true, _conf.ScreenCaptureMode);
		}

		public void CaptureLastRegion() {
			CaptureHelper.CaptureLastRegion(true);
		}

		public void CaptureIE() {
			if (_conf.IECapture) {
				CaptureHelper.CaptureIE(true, null);
			}
		}

		public void CaptureWindow() {
			if (_conf.CaptureWindowsInteractive) {
				CaptureHelper.CaptureWindowInteractive(true);
			} else {
				CaptureHelper.CaptureWindow(true);
			}
		}
		#endregion

		#region contextmenu
		void ContextMenuOpening(object sender, CancelEventArgs e)	{
			contextmenu_captureclipboard.Enabled = ClipboardHelper.ContainsImage();
			contextmenu_capturelastregion.Enabled = _conf.LastCapturedRegion != Rectangle.Empty;

			// IE context menu code
			try {
				if (_conf.IECapture && IECaptureHelper.IsIERunning()) {
					contextmenu_captureie.Enabled = true;
					contextmenu_captureiefromlist.Enabled = true;
				} else {
					contextmenu_captureie.Enabled = false;
					contextmenu_captureiefromlist.Enabled = false;
				}
			} catch (Exception ex) {
				_log.WarnFormat("Problem accessing IE information: {0}", ex.Message);
			}

			// Multi-Screen captures
			contextmenu_capturefullscreen.Click -= CaptureFullScreenToolStripMenuItemClick;
			contextmenu_capturefullscreen.DropDownOpening -= MultiScreenDropDownOpening;
			contextmenu_capturefullscreen.DropDownClosed -= MultiScreenDropDownClosing;
			if (Screen.AllScreens.Length > 1) {
				contextmenu_capturefullscreen.DropDownOpening += MultiScreenDropDownOpening;
				contextmenu_capturefullscreen.DropDownClosed += MultiScreenDropDownClosing;
			} else {
				contextmenu_capturefullscreen.Click += CaptureFullScreenToolStripMenuItemClick;
			}
		}
		
		void ContextMenuClosing(object sender, EventArgs e) {
			contextmenu_captureiefromlist.DropDownItems.Clear();
			contextmenu_capturewindowfromlist.DropDownItems.Clear();
			CleanupThumbnail();
		}
		
		/// <summary>
		/// Build a selectable list of IE tabs when we enter the menu item
		/// </summary>
		void CaptureIEMenuDropDownOpening(object sender, EventArgs e) {
			if (!_conf.IECapture) {
				return;
			}
			try {
				List<KeyValuePair<WindowDetails, string>> tabs = IECaptureHelper.GetBrowserTabs();
				contextmenu_captureiefromlist.DropDownItems.Clear();
				if (tabs.Count > 0) {
					contextmenu_captureie.Enabled = true;
					contextmenu_captureiefromlist.Enabled = true;
					var counter = new Dictionary<WindowDetails, int>();
					
					foreach(KeyValuePair<WindowDetails, string> tabData in tabs) {
						string title = tabData.Value;
						if (title == null) {
							continue;
						}
						if (title.Length > _conf.MaxMenuItemLength) {
							title = title.Substring(0, Math.Min(title.Length, _conf.MaxMenuItemLength));
						}
						ToolStripItem captureIETabItem = contextmenu_captureiefromlist.DropDownItems.Add(title);
						int index;
						if (counter.ContainsKey(tabData.Key)) {
							index = counter[tabData.Key];
						} else {
							index = 0;
						}
						captureIETabItem.Image = tabData.Key.DisplayIcon;
						captureIETabItem.Tag = new KeyValuePair<WindowDetails, int>(tabData.Key, index++);
						captureIETabItem.Click += Contextmenu_captureiefromlist_Click;
						contextmenu_captureiefromlist.DropDownItems.Add(captureIETabItem);
						if (counter.ContainsKey(tabData.Key)) {
							counter[tabData.Key] = index;
						} else {
							counter.Add(tabData.Key, index);
						}
					}
				} else {
					contextmenu_captureie.Enabled = false;
					contextmenu_captureiefromlist.Enabled = false;
				}
			} catch (Exception ex) {
				_log.WarnFormat("Problem accessing IE information: {0}", ex.Message);
			}
		}

		/// <summary>
		/// MultiScreenDropDownOpening is called when mouse hovers over the Capture-Screen context menu 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MultiScreenDropDownOpening(object sender, EventArgs e) {
			ToolStripMenuItem captureScreenMenuItem = (ToolStripMenuItem)sender;
			captureScreenMenuItem.DropDownItems.Clear();
			if (Screen.AllScreens.Length > 1) {
				Rectangle allScreensBounds = WindowCapture.GetScreenBounds();

				ToolStripMenuItem captureScreenItem = new ToolStripMenuItem(Language.GetString(LangKey.contextmenu_capturefullscreen_all));
				captureScreenItem.Click += delegate {
					BeginInvoke((MethodInvoker)(() => CaptureHelper.CaptureFullscreen(false, ScreenCaptureMode.FullScreen)));
				};
				captureScreenMenuItem.DropDownItems.Add(captureScreenItem);
				foreach (Screen screen in Screen.AllScreens) {
					Screen screenToCapture = screen;
					string deviceAlignment = "";
					if(screen.Bounds.Top == allScreensBounds.Top && screen.Bounds.Bottom != allScreensBounds.Bottom) {
						deviceAlignment += " " + Language.GetString(LangKey.contextmenu_capturefullscreen_top);
					} else if(screen.Bounds.Top != allScreensBounds.Top && screen.Bounds.Bottom == allScreensBounds.Bottom) {
						deviceAlignment += " " + Language.GetString(LangKey.contextmenu_capturefullscreen_bottom);
					}
					if(screen.Bounds.Left == allScreensBounds.Left && screen.Bounds.Right != allScreensBounds.Right) {
						deviceAlignment += " " + Language.GetString(LangKey.contextmenu_capturefullscreen_left);
					} else if(screen.Bounds.Left != allScreensBounds.Left && screen.Bounds.Right == allScreensBounds.Right) {
						deviceAlignment += " " + Language.GetString(LangKey.contextmenu_capturefullscreen_right);
					}
                    captureScreenItem = new ToolStripMenuItem(deviceAlignment);
					captureScreenItem.Click += delegate {
						BeginInvoke((MethodInvoker)(() => CaptureHelper.CaptureRegion(false, screenToCapture.Bounds)));
					};
					captureScreenMenuItem.DropDownItems.Add(captureScreenItem);
				}
			}
		}

		/// <summary>
		/// MultiScreenDropDownOpening is called when mouse leaves the context menu 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void MultiScreenDropDownClosing(object sender, EventArgs e) {
			ToolStripMenuItem captureScreenMenuItem = (ToolStripMenuItem)sender;
			captureScreenMenuItem.DropDownItems.Clear();
		}
		
		/// <summary>
		/// Build a selectable list of windows when we enter the menu item
		/// </summary>
		private void CaptureWindowFromListMenuDropDownOpening(object sender, EventArgs e) {
			// The Capture window context menu item used to go to the following code:
			// captureForm.MakeCapture(CaptureMode.Window, false);
			// Now we check which windows are there to capture
			ToolStripMenuItem captureWindowFromListMenuItem = (ToolStripMenuItem)sender;
			AddCaptureWindowMenuItems(captureWindowFromListMenuItem, Contextmenu_capturewindowfromlist_Click);
		}
		
		private void CaptureWindowFromListMenuDropDownClosed(object sender, EventArgs e) {
			CleanupThumbnail();
		}
		
		private void ShowThumbnailOnEnter(object sender, EventArgs e) {
			var captureWindowItem = sender as ToolStripMenuItem;
			if (captureWindowItem != null) {
				var window = captureWindowItem.Tag as WindowDetails;
				if (_thumbnailForm == null) {
					_thumbnailForm = new ThumbnailForm();
				}
				_thumbnailForm.ShowThumbnail(window, captureWindowItem.GetCurrentParent().TopLevelControl);
			}
		}

		private void HideThumbnailOnLeave(object sender, EventArgs e) {
			if (_thumbnailForm != null) {
				_thumbnailForm.Hide();
			}
		}
		
		private void CleanupThumbnail() {
			if (_thumbnailForm != null) {
				_thumbnailForm.Close();
				_thumbnailForm = null;
			}
		}

		public void AddCaptureWindowMenuItems(ToolStripMenuItem menuItem, EventHandler eventHandler) {
			menuItem.DropDownItems.Clear();
			// check if thumbnailPreview is enabled and DWM is enabled
			bool thumbnailPreview = _conf.ThumnailPreview && DWM.IsDwmEnabled();

			List<WindowDetails> windows = WindowDetails.GetTopLevelWindows();
			foreach(WindowDetails window in windows) {

				string title = window.Text;
				if (title != null) {
					if (title.Length > _conf.MaxMenuItemLength) {
						title = title.Substring(0, Math.Min(title.Length, _conf.MaxMenuItemLength));
					}
					ToolStripItem captureWindowItem = menuItem.DropDownItems.Add(title);
					captureWindowItem.Tag = window;
					captureWindowItem.Image = window.DisplayIcon;
					captureWindowItem.Click += eventHandler;
					// Only show preview when enabled
					if (thumbnailPreview) {
						captureWindowItem.MouseEnter += ShowThumbnailOnEnter;
						captureWindowItem.MouseLeave += HideThumbnailOnLeave;
					}
				}
			}
		}

		void CaptureAreaToolStripMenuItemClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureHelper.CaptureRegion(false);
			});
		}

		void CaptureClipboardToolStripMenuItemClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureHelper.CaptureClipboard();
			});
		}
		
		void OpenFileToolStripMenuItemClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureFile();
			});
		}

		void CaptureFullScreenToolStripMenuItemClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureHelper.CaptureFullscreen(false, _conf.ScreenCaptureMode);
			});
		}
		
		void Contextmenu_capturelastregionClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureHelper.CaptureLastRegion(false);
			});
		}
		
		void Contextmenu_capturewindow_Click(object sender,EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				CaptureHelper.CaptureWindowInteractive(false);
			});
		}

		void Contextmenu_capturewindowfromlist_Click(object sender,EventArgs e) {
			ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
			BeginInvoke((MethodInvoker)delegate {
				try {
					WindowDetails windowToCapture = (WindowDetails)clickedItem.Tag;
					CaptureHelper.CaptureWindow(windowToCapture);
				} catch (Exception exception) {
					_log.Error(exception);
				}
			});
		}
		
		void Contextmenu_captureie_Click(object sender, EventArgs e) {
			CaptureIE();
		}

		void Contextmenu_captureiefromlist_Click(object sender, EventArgs e) {
			if (!_conf.IECapture) {
				_log.InfoFormat("IE Capture is disabled.");
				return;
			}
			ToolStripMenuItem clickedItem = (ToolStripMenuItem)sender;
			KeyValuePair<WindowDetails, int> tabData = (KeyValuePair<WindowDetails, int>)clickedItem.Tag;
			BeginInvoke((MethodInvoker)delegate {
				WindowDetails ieWindowToCapture = tabData.Key;
				if (ieWindowToCapture != null && (!ieWindowToCapture.Visible || ieWindowToCapture.Iconic)) {
					ieWindowToCapture.Restore();
				}
				try {
					IECaptureHelper.ActivateIETab(ieWindowToCapture, tabData.Value);
				} catch (Exception exception) {
					_log.Error(exception);
				}
				try {
					CaptureHelper.CaptureIE(false, ieWindowToCapture);
				} catch (Exception exception) {
					_log.Error(exception);
				}
			});
		}

		/// <summary>
		/// Context menu entry "Support Greenshot"
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Contextmenu_donateClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				Process.Start("http://getgreenshot.org/support/");
			});
		}
		
		/// <summary>
		/// Context menu entry "Preferences"
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Contextmenu_settingsClick(object sender, EventArgs e) {
			BeginInvoke((MethodInvoker)delegate {
				ShowSetting();
			});
		}
		
		/// <summary>
		/// This is called indirectly from the context menu "Preferences"
		/// </summary>
		public void ShowSetting() {
			if (_settingsWindow == null) {
				try {
					_settingsWindow = new SettingsWindow("settings_general");
					ElementHost.EnableModelessKeyboardInterop(_settingsWindow);
					_settingsWindow.ShowDialog();
				} finally {
					_settingsWindow = null;
				}
			} else {
				_settingsWindow.Focus();
			}
		}
		
		/// <summary>
		/// The "About Greenshot" entry is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Contextmenu_aboutClick(object sender, EventArgs e) {
			ShowAbout();
		}

		public void ShowAbout() {
			if (_aboutWindow != null) {
				WindowDetails.ToForeground(_aboutWindow.Handle);
			} else {
				try {
					AboutWindow aboutWindow = new AboutWindow();
					_aboutWindow = new WindowInteropHelper(aboutWindow);
					ElementHost.EnableModelessKeyboardInterop(aboutWindow);
					aboutWindow.ShowDialog();
				} finally {
					_aboutWindow = null;
				}
			}
		}
		
		/// <summary>
		/// The "Help" entry is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Contextmenu_helpClick(object sender, EventArgs e) {
			HelpFileLoader.LoadHelp();
		}
		
		/// <summary>
		/// The "Exit" entry is clicked
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		void Contextmenu_exitClick(object sender, EventArgs e) {
			 Exit();
		}
		
		/// <summary>
		/// This needs to be called to initialize the quick settings menu entries
		/// </summary>
		private void InitializeQuickSettingsMenu() {
			contextmenu_quicksettings.DropDownItems.Clear();

			if (_conf.DisableQuickSettings) {
				return;
			}

			// Only add if the value is not fixed
			if (!_conf.Values["CaptureMousepointer"].IsFixed) {
				// For the capture mousecursor option
				var captureMouseItem = new ToolStripMenuSelectListItem {
					Text = Language.GetString("settings_capture_mousepointer"),
					Checked = _conf.CaptureMousepointer,
					CheckOnClick = true
				};
				captureMouseItem.CheckStateChanged += delegate {
					_conf.CaptureMousepointer = captureMouseItem.Checked;
				};

				contextmenu_quicksettings.DropDownItems.Add(captureMouseItem);
			}
			ToolStripMenuSelectList selectList;
			if (!_conf.Values["OutputDestinations"].IsFixed) {
				// screenshot destination
				selectList = new ToolStripMenuSelectList("destinations", true) {
					Text = Language.GetString(LangKey.settings_destination)
				};
				// Working with IDestination:
				foreach (var destination in DestinationHelper.GetAllDestinations()) {
					selectList.AddItem(destination.Description, destination, _conf.OutputDestinations.Contains(destination.Designation));
				}
				selectList.CheckedChanged += QuickSettingDestinationChanged;
				contextmenu_quicksettings.DropDownItems.Add(selectList);
			}

			if (!_conf.Values["WindowCaptureMode"].IsFixed) {
				// Capture Modes
				selectList = new ToolStripMenuSelectList("capturemodes", false) {
					Text = Language.GetString(LangKey.settings_window_capture_mode)
				};
				string enumTypeName = typeof(WindowCaptureMode).Name;
				foreach (WindowCaptureMode captureMode in Enum.GetValues(typeof(WindowCaptureMode))) {
					selectList.AddItem(Language.GetString(enumTypeName + "." + captureMode.ToString()), captureMode, _conf.WindowCaptureMode == captureMode);
				}
				selectList.CheckedChanged += QuickSettingCaptureModeChanged;
				contextmenu_quicksettings.DropDownItems.Add(selectList);
			}

			// print options
			selectList = new ToolStripMenuSelectList("printoptions",true) {
				Text = Language.GetString(LangKey.settings_printoptions)
			};

			IniValue iniValue;
			foreach(string propertyName in _conf.Values.Keys) {
				if (propertyName.StartsWith("OutputPrint")) {
					iniValue = _conf.Values[propertyName];
					if (iniValue.Attributes.LanguageKey != null && !iniValue.IsFixed) {
						selectList.AddItem(Language.GetString(iniValue.Attributes.LanguageKey), iniValue, (bool)iniValue.Value);
					}
				}
			}
			if (selectList.DropDownItems.Count > 0) {
				selectList.CheckedChanged += QuickSettingBoolItemChanged;
				contextmenu_quicksettings.DropDownItems.Add(selectList);
			}

			// effects
			selectList = new ToolStripMenuSelectList("effects",true) {
				Text = Language.GetString(LangKey.settings_visualization)
			};

			iniValue = _conf.Values["PlayCameraSound"];
			if (!iniValue.IsFixed) {
				selectList.AddItem(Language.GetString(iniValue.Attributes.LanguageKey), iniValue, (bool)iniValue.Value);
			}
			iniValue = _conf.Values["ShowTrayNotification"];
			if (!iniValue.IsFixed) {
				selectList.AddItem(Language.GetString(iniValue.Attributes.LanguageKey), iniValue, (bool)iniValue.Value);
			}
			if (selectList.DropDownItems.Count > 0) {
				selectList.CheckedChanged += QuickSettingBoolItemChanged;
				contextmenu_quicksettings.DropDownItems.Add(selectList);
			}
		}
		
		void QuickSettingCaptureModeChanged(object sender, EventArgs e) {
			ToolStripMenuSelectListItem item = ((ItemCheckedChangedEventArgs)e).Item;
			var windowsCaptureMode = (WindowCaptureMode)item.Data;
			if (item.Checked) {
				_conf.WindowCaptureMode = windowsCaptureMode;
			}
		}

		void QuickSettingBoolItemChanged(object sender, EventArgs e) {
			ToolStripMenuSelectListItem item = ((ItemCheckedChangedEventArgs)e).Item;
			IniValue iniValue = item.Data as IniValue;
			if (iniValue != null) {
				iniValue.Value = item.Checked;
				IniConfig.Save();
			}
		}

		void QuickSettingDestinationChanged(object sender, EventArgs e) {
			ToolStripMenuSelectListItem item = ((ItemCheckedChangedEventArgs)e).Item;
			var selectedDestination = (ILegacyDestination)item.Data;
			if (item.Checked) {
				if (selectedDestination.Designation.Equals(KnownDesignations.Picker.ToString())) {
					// If the item is the destination picker, remove all others
					_conf.OutputDestinations.Clear();
				} else {
					// If the item is not the destination picker, remove the picker
					_conf.OutputDestinations.Remove(KnownDesignations.Picker.ToString());
				}
				// Checked an item, add if the destination is not yet selected
				if (!_conf.OutputDestinations.Contains(selectedDestination.Designation)) {
					_conf.OutputDestinations.Add(selectedDestination.Designation);
				}
			} else {
				// deselected a destination, only remove if it was selected
				if (_conf.OutputDestinations.Contains(selectedDestination.Designation)) {
					_conf.OutputDestinations.Remove(selectedDestination.Designation);
				}
			}
			// Check if something was selected, if not make the picker the default
			if (_conf.OutputDestinations == null || _conf.OutputDestinations.Count == 0) {
				_conf.OutputDestinations.Add(KnownDesignations.Picker.ToString());
			}
			IniConfig.Save();

			// Rebuild the quick settings menu with the new settings.
			InitializeQuickSettingsMenu();
		}
		#endregion
		
		/// <summary>
		/// Handle the notify icon click
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NotifyIconClickTest(object sender, MouseEventArgs e) {
			if (e.Button != MouseButtons.Left) {
				return;
			}
			// The right button will automatically be handled with the context menu, here we only check the left.
			if (_conf.DoubleClickAction == ClickActions.DO_NOTHING) {
				// As there isn't a double-click we can start the Left click
				NotifyIconClick(_conf.LeftClickAction);
				// ready with the test
				return;
			}
			// If the timer is enabled we are waiting for a double click...
			if (_doubleClickTimer.Enabled) {
				// User clicked a second time before the timer tick: Double-click!
				_doubleClickTimer.Elapsed -= NotifyIconSingleClickTest;
				_doubleClickTimer.Stop();
				NotifyIconClick(_conf.DoubleClickAction);
			} else {
				// User clicked without a timer, set the timer and if it ticks it was a single click
				// Create timer, if it ticks before the NotifyIconClickTest is called again we have a single click
				_doubleClickTimer.Elapsed += NotifyIconSingleClickTest;
				_doubleClickTimer.Interval = SystemInformation.DoubleClickTime;
				_doubleClickTimer.Start();
			}
		}

		/// <summary>
		/// Called by the doubleClickTimer, this means a single click was used on the tray icon
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void NotifyIconSingleClickTest(object sender, EventArgs e) {
			_doubleClickTimer.Elapsed -= NotifyIconSingleClickTest;
			_doubleClickTimer.Stop();
			BeginInvoke((MethodInvoker)delegate {
				NotifyIconClick(_conf.LeftClickAction);
			});
		}

		/// <summary>
		/// Handle the notify icon click
		/// </summary>
		private void NotifyIconClick(ClickActions clickAction) {
			switch (clickAction) {
				case ClickActions.OPEN_LAST_IN_EXPLORER:
					string path = null;
					string configPath = FilenameHelper.FillVariables(_conf.OutputFilePath, false);
					string lastFilePath = Path.GetDirectoryName(_conf.OutputFileAsFullpath);
					if (lastFilePath != null && Directory.Exists(lastFilePath)) {
						path = lastFilePath;
					} else if (Directory.Exists(configPath)) {
						path = configPath;
					}

					if (path != null) {
						try {
							Process.Start(path);
						} catch (Exception ex) {
							// Make sure we show what we tried to open in the exception
							ex.Data.Add("path", path);
							throw;
						}
					}
					break;
				case ClickActions.OPEN_LAST_IN_EDITOR:
					if (File.Exists(_conf.OutputFileAsFullpath)) {
						CaptureHelper.CaptureFile(_conf.OutputFileAsFullpath, DestinationHelper.GetDestination(KnownDesignations.Editor));
					}
					break;
				case ClickActions.OPEN_SETTINGS:
					ShowSetting();
					break;
				case ClickActions.SHOW_CONTEXT_MENU:
					MethodInfo oMethodInfo = typeof(NotifyIcon).GetMethod("ShowContextMenu", BindingFlags.Instance | BindingFlags.NonPublic);
					oMethodInfo.Invoke(notifyIcon, null);
					break;
			}
		}

		/// <summary>
		/// The Contextmenu_OpenRecent currently opens the last know save location
		/// </summary>
		private void Contextmenu_OpenRecent(object sender, EventArgs eventArgs) {
			string path = FilenameHelper.FillVariables(_conf.OutputFilePath, false);
			// Fix for #1470, problems with a drive which is no longer available
			try {
				string lastFilePath = Path.GetDirectoryName(_conf.OutputFileAsFullpath);
				if (lastFilePath != null && Directory.Exists(lastFilePath)) {
					path = lastFilePath;
				} else if (!Directory.Exists(path)) {
					// What do I open when nothing can be found? Right, nothing...
					return;
				}
			} catch (Exception ex) {
				_log.Warn("Couldn't open the path to the last exported file, taking default.", ex);
			}
			_log.Debug("DoubleClick was called! Starting: " + path);
			try {
				Process.Start(path);
			} catch (Exception ex) {
				// Make sure we show what we tried to open in the exception
				ex.Data.Add("path", path);
				_log.Warn("Couldn't open the path to the last exported file", ex);
				// No reason to create a bug-form, we just display the error.
				MessageBox.Show(this, ex.Message, @"Opening " + path, MessageBoxButtons.OK, MessageBoxIcon.Error);
			}
		}
		
		/// <summary>
		/// Shutdown / cleanup
		/// </summary>
		public void Exit() {
			_log.Info("Exit: " + EnvironmentInfo.EnvironmentToString(false));
			WinEventHook.Instance.Dispose();

			// Close all open forms (except this), use a separate List to make sure we don't get a "InvalidOperationException: Collection was modified"
			List<Form> formsToClose = new List<Form>();
			foreach (Form form in Application.OpenForms) {
				if (form.Handle != Handle) {
					formsToClose.Add(form);
				}
			}
			foreach(Form form in formsToClose) {
				if (form == null) {
					continue;
				}
				try {
					_log.InfoFormat("Closing form: {0}", form.Name);
					Invoke((MethodInvoker) (form.Close));
				} catch (Exception e) {
					_log.Error("Error closing form!", e);
				}
			}
			
			// Make sure hotkeys are disabled
			try {
				SettingsHotkeyTextBox.UnregisterHotkeys();
			} catch (Exception e) {
				_log.Error("Error unregistering hotkeys!", e);
			}
	
			// Now the sound isn't needed anymore
			try {
				SoundHelper.Deinitialize();
			} catch (Exception e) {
				_log.Error("Error deinitializing sound!", e);
			}

			// Gracefull shutdown
			try {
				Application.DoEvents();
				Application.Exit();
			} catch (Exception e) {
				_log.Error("Error closing application!", e);
			}

			ImageOutput.RemoveTmpFiles();

			// Store any open configuration changes
			try {
				IniConfig.Save();
			} catch (Exception e) {
				_log.Error("Error storing configuration!", e);
			}

			// make the icon invisible otherwise it stays even after exit!!
			if (notifyIcon != null) {
				notifyIcon.Visible = false;
				notifyIcon.Dispose();
				notifyIcon = null;
			}
		}
		
		/// <summary>
		/// Do work in the background
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void BackgroundWorkerTimerTick(object sender, EventArgs e) {

			// Prevent multiple checks simultaneous
			// Force icon update, this is made to reduce Citrix issues
			Icon tmpIcon = notifyIcon.Icon;
			notifyIcon.Icon = null;
			notifyIcon.Icon = tmpIcon;

			// Do we need to minimize the working set?
			if (_conf.MinimizeWorkingSetSize) {
				_log.Info("Calling EmptyWorkingSet");
				PsAPI.EmptyWorkingSet(Process.GetCurrentProcess().Handle);
			}
		}
	}
}