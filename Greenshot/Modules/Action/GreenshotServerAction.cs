﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin;
using GreenshotPlugin.Core;
using GreenshotPlugin.Modules;
using log4net;
using System;
using System.ComponentModel.Composition;
using System.IO;
using System.Security.Principal;
using System.ServiceModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace Greenshot.Services {
	/// <summary>
	/// This startup action starts the Greenshot "server", which allows to open files etc.
	/// </summary>
	[Export(typeof(IStartupAction))]
	[ExportMetadata(ModuleExtensions.ACTIVE, true)]
	[ExportMetadata(ModuleExtensions.PRIORITY, -1)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
	public class GreenshotServerAction : IGreenshotContract, IStartupAction, IShutdownAction {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(GreenshotServerAction));
		private ServiceHost _host;

		public static string Identity {
			get {
				var windowsIdentity = WindowsIdentity.GetCurrent();
				if (windowsIdentity != null && windowsIdentity.User != null) {
					return windowsIdentity.User.ToString();
				}
				return null;
			}
		}
		public static string EndPoint {
			get {
				return "net.pipe://localhost/Greenshot/Greenshot_" + Identity;
			}
		}

		public async void Start() {
			LOG.Debug("Starting Greenshot server");
			await Task.Factory.StartNew(() => {
				_host = new ServiceHost(this, new[] { new Uri("net.pipe://localhost/Greenshot") });
				_host.AddServiceEndpoint(typeof(IGreenshotContract), new NetNamedPipeBinding(), "Greenshot_" + Identity);
				_host.Open();
			});
		}

		public void Shutdown() {
			LOG.Debug("Stopping Greenshot server");
			if (_host != null) {
				_host.Close();
				_host = null;
			}
		}

		#region IGreenshotContract
		public void Exit() {
			Application.Current.Shutdown();
		}

		public void ReloadConfig() {
			throw new NotImplementedException();
		}

		public void OpenFile(string filename) {
			LOG.InfoFormat("Open file requested: {0}", filename);
			if (File.Exists(filename)) {
				MainForm.Instance.BeginInvoke((System.Windows.Forms.MethodInvoker)(() => CaptureHelper.CaptureFile(filename)));
			} else {
				LOG.Warn("No such file: " + filename);
			}
		}
		#endregion
	}
}
