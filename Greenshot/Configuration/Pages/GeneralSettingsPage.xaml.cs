﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

using System.Windows;
using Greenshot.Helpers;
using Greenshot.IniFile;
using GreenshotPlugin.Core;
using GreenshotPlugin.Core.Settings;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using log4net;

namespace Greenshot.Configuration.Pages {
	/// <summary>
	/// Logic for the GeneralSettingsPage.xaml
	/// </summary>
	[Export(typeof(SettingsPage))]
	[ExportMetadata(SettingsPage.PATH, "settings_general")]
	public partial class GeneralSettingsPage : SettingsPage {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(GeneralSettingsPage));

		public IList<LanguageFile> Languages {
			get {
				return GreenshotPlugin.Core.Language.SupportedLanguages;
			}
		}

		public GeneralSettingsPage() {
			InitializeComponent();
		}

		private void SettingsPage_Loaded(object sender, RoutedEventArgs e) {
			if (IniConfig.IsPortable) {
				checkbox_autostartshortcut.Visibility = Visibility.Hidden;
				checkbox_autostartshortcut.IsChecked = false;
			} else {
				// Autostart checkbox logic.
				if (StartupHelper.HasRunAll()) {
					// Remove runUser if we already have a run under all
					StartupHelper.DeleteRunUser();
					checkbox_autostartshortcut.IsEnabled = StartupHelper.CanWriteRunAll();
					checkbox_autostartshortcut.IsChecked = true; // We already checked this
				} else if (StartupHelper.IsInStartupFolder()) {
					checkbox_autostartshortcut.IsEnabled = false;
					checkbox_autostartshortcut.IsChecked = true; // We already checked this
				} else {
					// No run for all, enable the checkbox and set it to true if the current user has a key
					checkbox_autostartshortcut.IsEnabled = StartupHelper.CanWriteRunUser();
					checkbox_autostartshortcut.IsChecked = StartupHelper.HasRunUser();
				}
			}
		}

		public override void Commit() {
			base.Commit();
			// Reflect Language change
			GreenshotPlugin.Core.Language.CurrentLanguage = CoreConfig.Language;
			if (MainForm.Instance != null) {
				MainForm.Instance.UpdateUI();
			} else {
				LOG.Warn("No GreenshotMain");
			}

			try {
				if (checkbox_autostartshortcut.IsChecked.HasValue && checkbox_autostartshortcut.IsChecked.Value) {
					// It's checked, so we set the RunUser if the RunAll isn't set.
					// Do this every time, so the executable is correct.
					if (!StartupHelper.HasRunAll()) {
						StartupHelper.SetRunUser();
					}
				} else {
					// Delete both settings if it's unchecked
					if (StartupHelper.HasRunAll()) {
						StartupHelper.DeleteRunAll();
					}
					if (StartupHelper.HasRunUser()) {
						StartupHelper.DeleteRunUser();
					}
				}
			} catch (Exception e) {
				LOG.Warn("Problem checking registry, ignoring for now: ", e);
			}
		}
	}
}
