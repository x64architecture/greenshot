﻿using GreenshotPlugin.UnmanagedHelpers;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Greenshot.Helpers {
	public class Arguments {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(Arguments));

		public List<string> FilesToOpen {
			get;
			set;
		}

		public bool IsExit {
			get;
			set;
		}
		public bool IsReload {
			get;
			set;
		}
		public bool IsNoRun {
			get;
			set;
		}
		public string Language {
			get;
			set;
		}
		public string IniDirectory {
			get;
			set;
		}

		public Arguments(string[] args) {
			FilesToOpen = new List<string>();

			if (args.Length > 0 && LOG.IsDebugEnabled) {
				StringBuilder argumentString = new StringBuilder();
				for (int argumentNr = 0; argumentNr < args.Length; argumentNr++) {
					argumentString.Append("[").Append(args[argumentNr]).Append("] ");
				}
				LOG.Debug("Greenshot arguments: " + argumentString);
			}

			for (int argumentNr = 0; argumentNr < args.Length; argumentNr++) {
				string argument = args[argumentNr];
				// Help
				if (argument.ToLower().Equals("/help") || argument.ToLower().Equals("/h") || argument.ToLower().Equals("/?")) {
					// Try to attach to the console
					bool attachedToConsole = Kernel32.AttachConsole(Kernel32.ATTACHCONSOLE_ATTACHPARENTPROCESS);
					// If attach didn't work, open a console
					if (!attachedToConsole) {
						Kernel32.AllocConsole();
					}
					var helpOutput = new StringBuilder();
					helpOutput.AppendLine();
					helpOutput.AppendLine("Greenshot commandline options:");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t/help");
					helpOutput.AppendLine("\t\tThis help.");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t/exit");
					helpOutput.AppendLine("\t\tTries to close all running instances.");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t/reload");
					helpOutput.AppendLine("\t\tReload the configuration of Greenshot.");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t/language [language code]");
					helpOutput.AppendLine("\t\tSet the language of Greenshot, e.g. greenshot /language en-US.");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t/inidirectory [directory]");
					helpOutput.AppendLine("\t\tSet the directory where the greenshot.ini should be stored & read.");
					helpOutput.AppendLine();
					helpOutput.AppendLine();
					helpOutput.AppendLine("\t[filename]");
					helpOutput.AppendLine("\t\tOpen the bitmap files in the running Greenshot instance or start a new instance");
					Console.WriteLine(helpOutput.ToString());

					// If attach didn't work, wait for key otherwise the console will close to quickly
					if (!attachedToConsole) {
						Console.ReadKey();
					}
					return;
				}

				if (argument.ToLower().Equals("/exit")) {
					IsExit = true;
					continue;
				}

				// Reload the configuration
				if (argument.ToLower().Equals("/reload")) {
					IsReload = true;
					continue;
				}

				// Stop running
				if (argument.ToLower().Equals("/norun")) {
					IsNoRun = true;
					continue;
				}

				// Language
				if (argument.ToLower().Equals("/language")) {
					Language = args[++argumentNr];
					continue;
				}

				// Setting the INI-directory
				if (argument.ToLower().Equals("/inidirectory")) {
					IniDirectory = args[++argumentNr];
					continue;
				}

				// Files to open
				FilesToOpen.Add(argument);
			}
		}
	}
}
