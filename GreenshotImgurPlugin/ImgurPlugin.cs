/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Greenshot.IniFile;
using GreenshotPlugin;
using GreenshotPlugin.Controls;
using GreenshotPlugin.Core;
using System.ComponentModel.Composition;
using log4net;
using System.Threading.Tasks;

namespace GreenshotImgurPlugin {
	/// <summary>
	/// This is the ImgurPlugin code
	/// </summary>
	[Export(typeof(IGreenshotPlugin))]
	[ExportMetadata("name", "Imgur plug-in")]
	public class ImgurPlugin : AbstractPlugin {
		private static readonly ILog LOG = LogManager.GetLogger(typeof(ImgurPlugin));
		private static ImgurConfiguration _config;
		private IGreenshotHost _host;
		private ComponentResourceManager _resources;
		private ToolStripMenuItem _historyMenuItem;

		protected override void Dispose(bool disposing) {
			if (!disposing) {
				return;
			}
			if (_historyMenuItem == null) {
				return;
			}
			_historyMenuItem.Dispose();
			_historyMenuItem = null;
		}

		public override IEnumerable<ILegacyDestination> Destinations() {
			yield return new ImgurDestination(this);
		}

		/// <summary>
		/// Implementation of the IGreenshotPlugin.Initialize
		/// </summary>
		/// <param name="pluginHost">Use the IGreenshotPluginHost interface to register events</param>
		/// <param name="metadata">IDictionary string, object</param>
		/// <returns>true if plugin is initialized, false if not (doesn't show)</returns>
		public override bool Initialize(IGreenshotHost pluginHost, IDictionary<string, object> metadata) {
			_host = pluginHost;

			// Get configuration
			_config = IniConfig.GetIniSection<ImgurConfiguration>();
			_resources = new ComponentResourceManager(typeof(ImgurPlugin));
			
			ToolStripMenuItem itemPlugInRoot = new ToolStripMenuItem("Imgur");
			itemPlugInRoot.Image = (Image)_resources.GetObject("Imgur");

			_historyMenuItem = new ToolStripMenuItem(Language.GetString("imgur", LangKey.history));
			_historyMenuItem.Tag = _host;
			_historyMenuItem.Click += delegate {
				ImgurHistory.ShowHistory();
			};
			itemPlugInRoot.DropDownItems.Add(_historyMenuItem);

			PluginUtils.AddToContextMenu(_host, itemPlugInRoot);
			Language.LanguageChanged += OnLanguageChanged;

			// retrieve history in the background
			Task.Factory.StartNew(() => {
				CheckHistory();
			});
			return true;
		}

		public void OnLanguageChanged(object sender, EventArgs e) {
			if (_historyMenuItem != null) {
				_historyMenuItem.Text = Language.GetString("imgur", LangKey.history);
			}
		}

		private void CheckHistory() {
			try {
				ImgurUtils.LoadHistory();
				if (PluginHelper.Instance.GreenshotMain != null) {
					((Control)_host.GreenshotMain).BeginInvoke((MethodInvoker)delegate {
						if (_config.ImgurUploadHistory.Count > 0) {
							_historyMenuItem.Enabled = true;
						} else {
							_historyMenuItem.Enabled = false;
						}
					});
				} else {
					LOG.Warn("No GreenshotMain");
				}
			} catch (Exception ex) {
				LOG.Error("Error loading history", ex);
			}
		}

		public override void Shutdown() {
			LOG.Debug("Imgur Plugin shutdown.");
			Language.LanguageChanged -= OnLanguageChanged;
		}

		/// <summary>
		/// Upload the capture to imgur
		/// </summary>
		/// <param name="captureDetails"></param>
		/// <param name="surfaceToUpload"></param>
		/// <param name="uploadURL">out string for the url</param>
		/// <returns>true if the upload succeeded</returns>
		public bool Upload(ICaptureDetails captureDetails, ISurface surfaceToUpload, out string uploadURL) {
			SurfaceOutputSettings outputSettings = new SurfaceOutputSettings(_config.UploadFormat, _config.UploadJpegQuality, _config.UploadReduceColors);
			try {
				string filename = Path.GetFileName(FilenameHelper.GetFilename(_config.UploadFormat, captureDetails));
				ImgurInfo imgurInfo = null;
			
				// Run upload in the background
				new PleaseWaitForm().ShowAndWait("Imgur plug-in", Language.GetString("imgur", LangKey.communication_wait), 
					delegate {
						imgurInfo = ImgurUtils.UploadToImgur(surfaceToUpload, outputSettings, captureDetails.Title, filename);
						if (imgurInfo != null && _config.AnonymousAccess) {
							LOG.InfoFormat("Storing imgur upload for hash {0} and delete hash {1}", imgurInfo.Hash, imgurInfo.DeleteHash);
							_config.ImgurUploadHistory.Add(imgurInfo.Hash, imgurInfo.DeleteHash);
							_config.runtimeImgurHistory.Add(imgurInfo.Hash, imgurInfo);
							CheckHistory();
						}
					}
				);

				if (imgurInfo != null) {
					// TODO: Optimize a second call for export
					using (Image tmpImage = surfaceToUpload.GetImageForExport()) {
						imgurInfo.Image = ImageHelper.CreateThumbnail(tmpImage, 90, 90);
					}
					IniConfig.Save();
					try {
						if (_config.UsePageLink) {
							uploadURL = imgurInfo.Page;
							ClipboardHelper.SetClipboardData(imgurInfo.Page);
						} else {
							uploadURL = imgurInfo.Original;
							ClipboardHelper.SetClipboardData(imgurInfo.Original);
						}
					} catch (Exception ex) {
						LOG.Error("Can't write to clipboard: ", ex);
						uploadURL = null;
					}
					return true;
				}
			} catch (Exception e) {
				LOG.Error("Error uploading.", e);
				MessageBox.Show(Language.GetString("imgur", LangKey.upload_failure) + " " + e.Message);
			}
			uploadURL = null;
			return false;
		}
	}
}
