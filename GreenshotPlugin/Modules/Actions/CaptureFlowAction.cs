﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace GreenshotPlugin.Modules.Actions {
	/// <summary>
	/// This IAction implements the basic capture flow:
	/// Source.import, Processor.process, Destination.export
	/// </summary>
	public class CaptureFlowAction : IAction {
		public ISource Source {
			get;
			set;
		}
		public IProcessor Processor {
			get;
			set;
		}
		public IDestination Destination {
			get;
			set;
		}
		public CaptureContext Context {
			get;
			set;
		}

		public void Execute() {
			if (Context == null) {
				throw new ArgumentException("Missing parameter", "Context");
			}
			Source.Import(Context);
			Processor.Process(Context);
			Destination.Export(Context);
		}

		public string Designation {
			get;
			set;
		}

		public string Description {
			get;
			set;
		}

		public int Priority {
			get;
			set;
		}

		public DependencyObject Visual {
			get;
			set;
		}

		public bool IsActive {
			get;
			set;
		}
	}
}
