﻿/*
 * Greenshot - a free and open source screenshot tool
 * Copyright (C) 2007-2014  Thomas Braun, Jens Klingen, Robin Krom
 * 
 * For more information see: http://getgreenshot.org/
 * The Greenshot project is hosted on Sourceforge: http://sourceforge.net/projects/greenshot/
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 1 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
using GreenshotPlugin.WPF;
using Svg2Xaml;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Reflection;
using System.Windows;
using System.Windows.Media;
using System.ComponentModel;
using System.Windows.Media.Imaging;

namespace GreenshotJiraPlugin {
	/// <summary>
	/// Interaction logic for JiraUploadWindow.xaml
	/// </summary>
	public partial class JiraUploadWindow : Window, INotifyPropertyChanged {
		public static DrawingImage JiraLogo {
			get {
				return JiraPlugin.JiraLogo;
			}
		}
		public static BitmapSource JiraIcon {
			get {
				return JiraPlugin.JiraIcon;
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		/// <summary>
		/// The issue property
		/// </summary>
		private Issue _selectedIssue;

		public List<FilterFavourite> Filters {
			get;
			private set;
		}

		public ObservableCollection<Issue> Issues {
			get;
			private set;
		}

		public Issue SelectedIssue {
			get {
				return _selectedIssue;
			}
			private set {
				_selectedIssue = value;
				if (PropertyChanged != null) {
					PropertyChanged(this, new PropertyChangedEventArgs("SelectedIssue"));
				}
			}
		}

		public string Comment {
			get;
			private set;
		}

		/// <summary>
		/// Replace this with binding to the filename in the CaptureContext, as soon as the ISurface is gone
		/// </summary>
		public string Filename {
			get;
			private set;
		}

		public JiraUploadWindow(JiraRest jira) {
			Filters = jira.Filters().Result;
			Issues = new ObservableCollection<Issue>();
			var searchResult = jira.Find(Filters[0].Jql).Result;
			foreach (var issue in searchResult.Issues) {
				Issues.Add(issue);
			}

			InitializeComponent();
			DataContext = this;
		}

		private void Button_Click(object sender, RoutedEventArgs e) {
			Close();
		}
	}
}
